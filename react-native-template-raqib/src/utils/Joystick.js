const __ = string => {
  return string;
};

const Log = {
  info: message => {
    // show message on green background
  },
  warning: message => {
    // show message on yellow background
  },
};

const confirm = message => {
  // show dialog with message and "Ok" "Cancel" buttons
  let result = true; // true if user press Ok, and false if user press Cancel
  return result;
};

const abort = () => {
  // abort previous send
};
const send = async command => {
  // async send command to server
  // result consist parsed json response from server
  let result = {};
  return result;
};

const Joystick = {
  hwjCalibratorId: null,
  obj: null,
  priority: null,
  lock_incheck: false,
  userid: null,
  commandPreset: null,
  speed: 1, // current move speed, can be changed by the sensitivity slider
  axisState: { '0': 0, '1': 0 },
  zoomAxis: 3, // Zoom axis; needed to quickly find value of zoom axis when moving x or y to provide simultaneous zooming and moving

  /* Speed commands support */
  speedCommandDelay: 500, // Delay between two consecutive calls of sendModeSpeedCommand
  minDelayBetween2Command: 100, // minimum delay between two consecutive calls of sendModeSpeedCommand
  isCommandPending: false, // pending speed command flag
  lastCommandSent: 0, // time when last ajax command was sent to ptz server
  timerSendSpeedCommand: null,

  isDebug: false,

  defaultPreset: {
    button: {
      '0': 'mode=step&zoom=in',
      '1': 'mode=step&zoom=out',
      '2': 'mode=preset&goto=1',
      '3': 'mode=preset&goto=2',
      '4': 'mode=preset&goto=3',
      '5': 'mode=preset&goto=4',
      '6': 'mode=preset&goto=5',
      '7': 'mode=preset&goto=6',
      '8': 'mode=preset&goto=7',
      '9': 'mode=preset&goto=8',
      '10': 'mode=preset&goto=9',
      '11': '',
      '12': '',
      '99': '', // do not remove
    },
    axis: {
      '1': '-tilt',
      '3': 'mode=speed&zoom=',
    },
    deadzone: {
      default: 5,
    },
  },

  /**
   * list of all command presets available
   * @let {Object}
   */
  commandPresets: {},

  /**
   * HWJ parameters
   * @let {Object}
   */
  parameters: { name: '', axis: [], button: [] },

  /**
   * initialization of HWJ
   *
   * @returns {Deferred}
   */
  init: function() {
    // 'hwjload', self.onHWJLoad;
    // 'axis', self.onAxis;
    // 'hat', self.onHat;
    // 'button', self.onButton;
    // 'enddeadzonecalibrate', self.onEndDeadZoneCalibrate;
    // 'log', self.onLog;

    Joystick.onHWJLoad(true, Joystick.parameters);
  },

  /**
   * load command presets from db, check current joystick name and set appropriate command preset
   *
   * @returns {Promise}
   */
  loadCommandPresets: function() {
    // return new Promise((resolve, reject) => {
    //   let api = new API();
    //   api
    //     .getIdentityAttribute({
    //       attribute: 'JOYSTICK_PRESETS',
    //     })
    //     .done(function(response) {
    //       if (response.value) {
    //         Joystick.commandPresets = JSON.parse(response.value);
    //       }
    //     })
    //     .always(function() {
    //       // get joystick name and search it in command presets
    //       let info = Joystick.getInfo();
    //       let preset = Joystick.defaultPreset;
    //       if (Joystick.commandPresets[info.name]) {
    //         preset = Joystick.commandPresets[info.name];
    //       }
    //       Joystick.setCommandPreset(preset);
    //
    //       resolve();
    //     });
    // });
  },

  /**
   * Set current preset for joystick commands
   *
   * @param {Object} commandPreset
   */
  setCommandPreset: function(commandPreset) {
    // Clone object
    this.commandPreset = Object.assign({}, commandPreset);

    let info = Joystick.getInfo();
    for (let name in info.axis) {
      if (!info.axis.hasOwnProperty(name)) continue;

      let value = 0; // info.axis[i].value

      // set speed value accordingly to current axis position if sensitivity assigned for some axis
      if (
        Joystick.commandPreset.axis[name] &&
        Joystick.commandPreset.axis[name].indexOf('sensitivity') >= 0
      ) {
        Joystick.setSpeed(
          Joystick.commandPreset.axis[name],
          Joystick.axisState[name]
        );
      }

      // Find zooming axis
      if (
        Joystick.commandPreset.axis[name] &&
        Joystick.commandPreset.axis[name].indexOf('mode=speed&zoom=') >= 0
      ) {
        Joystick.zoomAxis = name;
        Joystick.axisState[Joystick.zoomAxis] = 0;
      }
    }

    // if preset contains DeadZone settings, send them to applet
    if (Joystick.commandPreset.deadzone) {
      Joystick.setDeadZone(Joystick.commandPreset.deadzone);
    }
  },

  /**
   * remove HWJ from page
   */
  destroy: function() {},

  /**
   * get joystick info
   *
   * @returns {Object} joystick info
   */
  getInfo: function() {
    return Joystick.parameters;
  },

  /**
   * set obj of PTZ device in ssytem
   *
   * @param {Number} obj
   */
  setObj: function(obj) {
    Joystick.obj = obj;
    if (obj) {
      Joystick.priority = Joystick.getPTZPriority();
    }
  },

  /**
   *
   * @returns {Number}
   */
  getPTZPriority: function() {
    let priority = 1;

    // let api = new API(true);
    // api
    //   .getObjectList({
    //     type: 'role',
    //     withAttributes: true,
    //   })
    //   .done(function(response) {
    //     for (let i = 0; i < response.list.length; i++) {
    //       api
    //         .getAttribute({
    //           obj: response.list[i].obj,
    //           attribute: 'PTZ_PRIORITY',
    //         })
    //         .done(function(response) {
    //           if (response.value > priority) {
    //             priority = response.value;
    //           }
    //         });
    //     }
    //   });

    return priority;
  },

  /**
   * Send info about deadzone to applet
   *
   * @param {Object} axis data for deadzone, ex. {0: 0.1, 1: 0.05, 2: 0}
   */
  setDeadZone: function(axis) {
    if (Joystick.hwj) {
      Joystick.hwj.setDeadZone(axis);
    }
  },

  /**
   * Set speed of joystick accordingly to value of sensitivity axis
   *
   * @param {String} sens sensitivity|-sensitivity, how to compute speed
   * @param {Number} value value of axis
   */
  setSpeed: function(sens, value) {
    // Reversed sensitivity
    if (sens == '-sensitivity') {
      value = -value;
    }

    // axis returns values -100..100, we need values 0.1 .. 1
    value = (value + 100) / 200;
    if (value < 0.1) value = 0.1;
    Joystick.speed = value;
  },

  /**
   * send PTZ command to server, and call callback function after server response or error
   *
   * @param {string} command for send
   * @param {boolean} [addDev] add dev or not
   * @param {number} [priority]
   * @returns {Deferred}
   */
  sendCommand: function(command, addDev, priority) {
    addDev = typeof addDev == 'undefined' ? true : !!addDev;
    priority = priority || Joystick.priority;

    return new Promise(async (resolve, reject) => {
      if (addDev && !(Joystick.obj && command)) {
        reject();
        return;
      }

      // mode=speed&pt=0,0
      Joystick.isCommandPending = true;
      Joystick.lastCommandSent = new Date().getTime();

      let devString = addDev ? 'cam_objid=' + Joystick.obj + '&' : '';
      command = devString + command + '&priority=' + priority;
      command = command.replace(/--/g, ''); // delete '--' to provide "reverse" behaviour

      if (Joystick.isDebug) {
        console.log('Send: ' + command);
      }

      return resolve(command);
      /*
            console.log(command);
            Joystick.isCommandPending = false;
            */

      // abort previous command before sending new one

      // abort();
      // try {
      //   let data = await send('/api/ptz?' + command);
      //   // check priority lock
      //   if (data.lockinfo && data.lockinfo.allow_override != undefined) {
      //     Joystick.checkLock(data.lockinfo.allow_override);
      //   }
      // } catch (e) {}
      //
      // Joystick.isCommandPending = false;
      // resolve();
    });
  },

  /**
   *
   * @param {Number} x
   * @param {Number} y
   * @param {Number} width
   * @param {Number} height
   */
  gotoXY: function(x, y, width, height) {
    Joystick.sendCommand(
      'mode=rel&size=' + width + 'x' + height + '&xy=' + x + ',' + y
    );
  },

  /**
   * check lock
   *
   * @param {String} lockOverride lock override state
   */
  checkLock: function(lockOverride) {
    if (Joystick.lock_incheck) {
      return;
    }

    Joystick.lock_incheck = true;

    switch (lockOverride) {
      case 'sameuser':
        if (window.mx) window.mx.PTZ.lockDevice();
        break;
      case 'no':
        Log.info(
          __(
            'Another lock is higher priority. You have to wait until controls are released. Please try again later.'
          )
        );
        break;
      case 'yes':
        let getLock = confirm(
          __('Another lock is in place. Do you want to override?')
        );
        if (getLock) {
          Joystick.overrideLock();
          if (window.mx) window.mx.PTZ.lockDevice();
        }
        break;
    }

    Joystick.lock_incheck = false;
  },

  /**
   * override lock
   */
  overrideLock: function() {
    Joystick.sendCommand('mode=lock&cmd=override');
  },

  /**
   * Send speed command; when user use axis, this function calls every speedCommandDelay milliseconds
   * until user releases joystick.
   * Every 5 iterations it checks joystick position to prevent endless movement if we skipped "stop" command for some reason.
   *
   * Next speed command can be sent only if:
   * 1) speedCommandDelay milliseconds passed since last command was sent;
   * 2) last ajax command was completed
   *
   * @param {String} command    speed command
   * @param {Number} iteration  number of function iteration
   */
  sendModeSpeedCommand: function(command, iteration) {
    if (!Joystick.obj) return;

    /*
        // Once in a 10 iterations check current position of axis to prevent endless movement
        // (if last event from joystick with axis=0 was lost)
        // TODO: check code and remove it
        if (iteration > 5)
        {
            let info = Joystick.getInfo();
            for (let i in info.axis)
            {
                if (!info.axis.hasOwnProperty(i))
                    continue;

                // If axis state changed send event
                if (Joystick.axisState[info.axis[i].name] && Joystick.axisState[info.axis[i].name] != info.axis[i].value)
                {
                    Joystick.onAxis(info.axis[i].name, info.axis[i].value);
                }
            }
            return;
        }
        */

    // if previous command completed,
    // and
    // delay time passed or this new command
    // we can send another command
    if (
      !Joystick.isCommandPending
      //&&
      //(new Date().getTime() - Joystick.lastCommandSent > Joystick.minDelayBetween2Command)
    ) {
      Joystick.sendCommand(command);
    }

    Joystick.stopSendSpeedCommand();
    Joystick.timerSendSpeedCommand = setTimeout(function() {
      Joystick.sendModeSpeedCommand(command, iteration + 1);
    }, Joystick.speedCommandDelay);
  },

  /**
   * Clear sendModeSpeedCommand timeout
   */
  stopSendSpeedCommand: function() {
    if (Joystick.timerSendSpeedCommand !== null) {
      clearTimeout(Joystick.timerSendSpeedCommand);
    }
  },

  /**
   * Function is called when user use joystick axis
   *
   * @param {String} axis   name of axis
   * @param {Number} value  value of axis
   */
  onAxis: function(axis, value) {
    if (Joystick.isDebug) {
      console.log(axis, value);
    }
    Joystick.axisState[axis] = value;

    // Check for reversed Y behaviour
    if (
      axis == 1 &&
      Joystick.commandPreset.axis[1] &&
      Joystick.commandPreset.axis[1] == '-tilt'
    ) {
      Joystick.axisState[axis] = -value;
    }

    let command = '';
    let isStop = false;

    if (axis == 0 || axis == 1) {
      // for ptz command: || axis === Joystick.zoomAxis
      command =
        'mode=speed&pt=' +
        Math.round(Joystick.axisState['0'] * Joystick.speed) +
        ',' +
        Math.round(Joystick.axisState['1'] * Joystick.speed);
      // for ptz command: Math.round(Joystick.axisState[Joystick.zoomAxis] * Joystick.speed);

      if (Joystick.axisState['0'] == 0 && Joystick.axisState['1'] == 0) {
        // for ptz command: && Joystick.axisState[Joystick.zoomAxis] === 0
        isStop = true;
      }
    } else if (Joystick.commandPreset.axis[axis]) {
      // Sensitivity command
      if (Joystick.commandPreset.axis[axis].indexOf('sensitivity') != -1) {
        Joystick.setSpeed(Joystick.commandPreset.axis[axis], value);
        return;
      }

      command =
        Joystick.commandPreset.axis[axis] +
        Math.round(Joystick.axisState[axis] * Joystick.speed);
      if (Joystick.axisState[axis] == 0) {
        isStop = true;
      }
    }

    return Joystick.sendCommand(command);
  },

  /**
   *
   * @param {String} hat
   * @param {Number} value
   */
  onHat: function(hat, value) {},

  /**
   * Function is called when user click on joystick button
   *
   * @param {String} button joystick button name
   * @param {Number} value
   */
  onButton: function(button, value) {
    if (value == 1 && Joystick.commandPreset.button[button]) {
      Joystick.sendCommand(Joystick.commandPreset.button[button]);
    }
  },

  /**
   *
   * @param {Object} parameters
   */
  onEndDeadZoneCalibrate: function(parameters) {},

  /**
   *
   * @param {Number} code
   * @param {String} message
   */
  onLog: function(code, message) {
    switch (code) {
      case 0:
        Log.info(message);
        break;
      case 1:
        Log.warning(message);
        break;
    }
  },

  /**
   * @param {Boolean} success
   */
  onLoad: function(success) {},

  /**
   *
   * @param {Boolean} success
   * @param {Object} parameters
   */
  onHWJLoad: function(success, parameters) {
    if (success) {
      // Set default command preset
      Joystick.setCommandPreset(Joystick.defaultPreset);

      Joystick.parameters = parameters;

      Joystick.onLoad(success);
    }
  },
};

export default Joystick;
