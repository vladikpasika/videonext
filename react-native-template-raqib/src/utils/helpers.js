import { Dimensions, PixelRatio } from 'react-native';
import sha512 from 'js-sha512';

function getEncryptionKey(key, password) {
  let hash = sha512.create();
  hash.update(key + sha512(password) + key);
  hash.hex();
  return hash.toString();
}

function getDimensions() {
  return Dimensions.get('window');
}

function isSystemSaved(systems, sysAddress) {
  const url = sysAddress.replace(/(^\w+:|^)\/\//, '');
  let isSaved = false;
  systems.forEach(function(item) {
    const savedUrl = item.sysAddress.replace(/(^\w+:|^)\/\//, '');
    if (url === savedUrl) {
      isSaved = true;
    }
  });
  return isSaved;
}

function JSONstringify(json) {
  if (typeof json !== 'string') {
    json = JSON.stringify(json, undefined, '\t');
  }

  var arr = [],
    _string = 'color:green',
    _number = 'color:darkorange',
    _boolean = 'color:blue',
    _null = 'color:magenta',
    _key = 'color:red';

  json = json.replace(
    /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
    function(match) {
      var style = _number;
      if (/^"/.test(match)) {
        if (/:$/.test(match)) {
          style = _key;
        } else {
          style = _string;
        }
      } else if (/true|false/.test(match)) {
        style = _boolean;
      } else if (/null/.test(match)) {
        style = _null;
      }
      arr.push(style);
      arr.push('');
      return '%c' + match + '%c';
    }
  );

  arr.unshift(json);

  console.log.apply(console, arr);
}

const scale = Dimensions.get('window').width / 375;

const normalize = size => {
  if (!size) {
    return 0;
  }
  // if (Platform.OS === 'ios') {
  // 	return Math.round(PixelRatio.roundToNearestPixel(scale * size));
  // }
  return Math.ceil(PixelRatio.roundToNearestPixel(scale * size));
};

export {
  getEncryptionKey,
  getDimensions,
  isSystemSaved,
  JSONstringify,
  normalize,
};
