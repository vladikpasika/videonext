import { put, takeLatest, call, select, takeEvery } from 'redux-saga/effects';
import moment from 'moment';
//Actions
import {
  types,
  successCurrentCameraSet,
  successEventLog,
  failureEventLog,
  successCameraAttributes,
  failtureCameraAttributes,
  successSets,
  failtureSets,
  getMediaUrlSuccess,
  getMediaUrlFailture,
  successCameraEventLog,
  failureCameraEventLog,
  ptzEventSaveRequestId,
} from '../actions/cameras';
//Api request wrapper
import { api } from '../services/api';
//Selectors
import {
  getCurrentRoleId,
  getCurrentCamera,
  getCurrentCameraTime,
  getCurrentSet,
  getPtzLastRequestId,
  getCamerasList,
  getServerAddress,
} from './selectors';
//Api endpoints
import {
  ENDPOINT_GET_OBJECT_LIST,
  ENDPOINT_EVENT_LOG,
  ENDPOINT_GET_MEDIA_URL,
  ENDPOINT_PTZ_SEND_COMMAND,
} from '../constants';

function* getObjectListRequest(payload) {
  const serverAddress = yield select(getServerAddress);
  return yield call(
    api,
    serverAddress,
    ENDPOINT_GET_OBJECT_LIST,
    'GET',
    payload
  );
}

export function* camerasSet({ payload }) {
  try {
    const data = yield getObjectListRequest({
      type: 'camera',
      withAttributes: true,
      setid: payload,
    });

    yield put(
      successCurrentCameraSet({
        list: data.data.list.reduce(
          (prevItem, item) => ({ ...prevItem, [item.obj]: item }),
          {}
        ),
        currentSet: payload,
      })
    );
  } catch (err) {
    console.log(err);
  }
}

function* cameraAttributes() {
  try {
    const cameras = yield getObjectListRequest({
      type: 'camera',
      withAttributes: true,
    });
    yield put(
      successCameraAttributes(
        cameras.data.list.reduce(
          (prevItem, item) => ({ ...prevItem, [item.obj]: item }),
          {}
        )
      )
    );
  } catch (err) {
    yield put(failtureCameraAttributes());
  }
}

function* setsInfo() {
  const roleid = yield select(getCurrentRoleId);
  const setid = yield select(getCurrentSet);

  try {
    const sets = yield getObjectListRequest({
      type: 'set',
      setid,
      roleid,
    });
    yield put(
      successSets(
        sets.data.list
          .filter(function(item) {
            return item.protected === '0' || item.name === 'All Cameras';
          })
          .reduce((prevItem, item) => ({ ...prevItem, [item.obj]: item }), {})
      )
    );
  } catch (err) {
    yield put(failtureSets());
  }
}

function* mediaUrl() {
  const currentCameraId = yield select(getCurrentCamera);
  const currentCameraTime = yield select(getCurrentCameraTime);
  let data = { cameraid: currentCameraId };
  const serverAddress = yield select(getServerAddress);
  if (currentCameraTime !== null) {
    const { startTime, endTime } = currentCameraTime;
    data = { cameraid: currentCameraId, startTime, endTime };
  }
  try {
    const mediaUrl = yield call(
      api,
      serverAddress,
      ENDPOINT_GET_MEDIA_URL,
      'GET',
      data
    );
    yield put(getMediaUrlSuccess(mediaUrl.data.url));
  } catch (err) {
    yield put(getMediaUrlFailture());
  }
}

function* cameraEventLog({ payload, meta }) {
  let pagebreak = null;
  const serverAddress = yield select(getServerAddress);
  if ((payload && meta === 'loadLogs') || (payload && meta === 'updateLogs')) {
    pagebreak = {
      pagesize: payload.size,
      page: payload.page,
      sort: payload.sort,
      sortasc: payload.sortasc,
    };
  }
  const currentCameraId = yield select(getCurrentCamera);
  try {
    const logs = yield call(api, serverAddress, ENDPOINT_EVENT_LOG, 'GET', {
      filter: {
        filter: {
          objid: [currentCameraId],
        },
        pagebreak,
      },
    });
    yield put(
      successCameraEventLog(
        logs.data.reduce((prevItem, item) => {
          const convertDate = moment(item.when);
          return Object.assign({}, prevItem, {
            [item.eventId]: {
              ...item,
              when: {
                date: convertDate.format('DD/MM/YYYY'),
                time: convertDate.format('hh:mm:ss A'),
              },
            },
          });
        }, {}),
        meta
      )
    );
  } catch (err) {
    yield put(failureCameraEventLog());
  }
}

function* eventLog({ payload, meta }) {
  let pagebreak = null;
  const serverAddress = yield select(getServerAddress);
  const currentSet = yield select(getCurrentSet);
  if ((payload && meta === 'loadLogs') || (payload && meta === 'updateLogs')) {
    pagebreak = {
      pagesize: payload.size,
      page: payload.page,
      sort: payload.sort,
      sortasc: payload.sortasc,
    };
  }
  const roleid = yield select(getCurrentRoleId);
  try {
    const cameras = yield getObjectListRequest({
      type: 'camera',
      withAttributes: true,
      setid: currentSet,
    });
    yield put(
      successCurrentCameraSet({
        list: cameras.data.list.reduce(
          (prevItem, item) => ({ ...prevItem, [item.obj]: item }),
          {}
        ),
        currentSet,
      })
    );
    const currentCameras = yield select(getCamerasList);
    console.log(currentCameras);
    const camerasList = cameras.data.list.reduce((prevItem, item) => {
      if (currentCameras[item.obj]) {
        return { ...prevItem, [item.obj]: item };
      }
      return prevItem;
    }, {});
    const objIds = Object.keys(camerasList);
    let logs = [];
    if (objIds.length) {
      const { data } = yield call(
        api,
        serverAddress,
        ENDPOINT_EVENT_LOG,
        'GET',
        {
          filter: {
            filter: {
              objid: objIds,
            },
            pagebreak,
          },
          roleid,
        }
      );
      logs = data.reduce((prevItem, item) => {
        const convertDate = moment(item.when);
        return Object.assign({}, prevItem, {
          [item.eventId]: {
            ...item,
            when: {
              date: convertDate.format('DD/MM/YYYY'),
              time: convertDate.format('hh:mm:ss A'),
            },
          },
        });
      }, {});
    }

    yield put(
      successEventLog(
        {
          eLogs: logs,
          camerasList,
        },
        meta
      )
    );
  } catch (err) {
    yield put(failureEventLog());
  }
}

function* ptzEventRequestSaga(payload) {
  const { command } = payload;
  const serverAddress = yield select(getServerAddress);
  const lastRequestId = yield select(getPtzLastRequestId);
  const requestId = Math.ceil(Math.random() * 100000000);
  yield put(ptzEventSaveRequestId({ requestId }));
  yield call(
    api,
    serverAddress,
    ENDPOINT_PTZ_SEND_COMMAND + command,
    'GET',
    null,
    requestId,
    lastRequestId
  );
}

function* watchGetCurrentCameraSet() {
  yield takeLatest(types.GET_CURRENT_CAMERA_SET.REQUEST, camerasSet);
}

function* watchGetEventLog() {
  yield takeLatest(types.GET_EVENT_LOG.REQUEST, eventLog);
}

function* watchGetCameraAttributes() {
  yield takeLatest(types.GET_CAMERA_ATTRIBUTES.REQUEST, cameraAttributes);
}

function* watchGetSets() {
  yield takeLatest(types.GET_SETS.REQUEST, setsInfo);
}

function* watchGetMediaUrl() {
  yield takeLatest(types.GET_MEDIA_URL.REQUEST, mediaUrl);
}

function* watchGetCameraEventLog() {
  yield takeLatest(types.GET_CAMERA_EVENT_LOG.REQUEST, cameraEventLog);
}

function* watchPtzEventRequestSaga() {
  yield takeEvery(types.PTZ_EVENT.REQUEST, ptzEventRequestSaga);
}

export {
  watchGetEventLog,
  watchGetCameraAttributes,
  watchGetSets,
  watchGetMediaUrl,
  watchGetCurrentCameraSet,
  watchGetCameraEventLog,
  watchPtzEventRequestSaga,
};
