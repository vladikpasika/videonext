export const getCameras = state => state.cameras.list;

export const getCurrentRoleId = state => state.user.userData.info.role.obj;

export const getCurrentSet = state => state.cameras.currentSet;

export const getCurrentCamera = state => state.cameras.currentCamera;

export const getCurrentCameraTime = state => state.cameras.currentCameraTime;

export const getEventLogs = state => state.cameras.eventLogs;

export const getPtzLastRequestId = state => state.cameras.ptz.lastRequestId;

export const getCamerasList = state => state.cameras.list;

export const getServerAddress = state => state.user.serverAddress;

export const getServerName = state => state.user.serverName;
