import { put, takeLatest, call, select } from 'redux-saga/effects';
import {
  types,
  loginInfoSuccess,
  loginInfoFailure,
  loginSuccess,
  loginFailture,
  logoutSuccess,
} from '../actions/userAction';
import { resetCameras } from '../actions/cameras';
import { api } from '../services/api';
import { getEncryptionKey } from '../utils/helpers';
import { getServerAddress } from './selectors';
import { createdPersistor } from '../../App';
import {
  ENDPOINT_LOGIN_INFO,
  ENDPOINT_LOGIN,
  ENDPOINT_LOGOUT,
} from '../constants';

function* loginSaga(userData) {
  const { userName, password, loggedIn, sysName, sysAddress } = userData;
  try {
    if (loggedIn) {
      yield call(api, sysAddress, ENDPOINT_LOGOUT, 'GET');
      yield put(resetCameras());
    }
    const info = yield call(api, sysAddress, ENDPOINT_LOGIN_INFO, 'GET');
    if (info.type === 'error') {
      yield put(loginInfoFailure(info.message));
      return;
    }
    yield put(loginInfoSuccess(info));
    const key = getEncryptionKey(
      info.data.loginInfo.encryptionKey,
      userData.password
    );
    const loginResponse = yield call(api, sysAddress, ENDPOINT_LOGIN, 'POST', {
      credentials: key,
      password,
      name: userName,
    });
    if (loginResponse.type === 'error') {
      yield put(loginFailture(loginResponse.message));
      return;
    }
    const systemAddress = sysAddress;
    const systemName = sysName;
    console.log({
      sysName: systemName,
      sysAddress: systemAddress,
      userName,
      password,
      data: loginResponse.data,
      systemInfo: {
        sysName: systemName,
        sysAddress: systemAddress,
        userName,
        password,
        userData: loginResponse.data,
      },
    });
    yield put(
      loginSuccess({
        sysName: systemName,
        sysAddress: systemAddress,
        userName,
        password,
        data: loginResponse.data,
        systemInfo: {
          sysName: systemName,
          sysAddress: systemAddress,
          userName,
          password,
          userData: loginResponse.data,
        },
      })
    );
  } catch (err) {
    // alert(err.message);
    console.log('LOGIN_ERR', err.message);
  }
}

function* backgroundAuthSaga(data) {
  const { userName, password } = data;
  const serverAddress = yield select(getServerAddress);
  try {
    yield call(api, serverAddress, ENDPOINT_LOGOUT, 'GET');
    const info = yield call(api, serverAddress, ENDPOINT_LOGIN_INFO, 'GET');
    yield put(loginInfoSuccess(info));
    const key = getEncryptionKey(info.data.loginInfo.encryptionKey, password);
    yield call(api, serverAddress, ENDPOINT_LOGIN, 'POST', {
      credentials: key,
      password,
      name: userName,
    });
  } catch (err) {
    console.log('backgroundAuthSaga', err.message);
  }
}

function* logoutSaga() {
  const serverAddress = yield select(getServerAddress);
  try {
    const responce = yield call(api, serverAddress, ENDPOINT_LOGOUT, 'GET');
    if (responce) {
      yield createdPersistor.purge();
      yield put(logoutSuccess(responce));
    }
  } catch (err) {
    console.log('logout', err);
  }
}

function* watchLoginSaga() {
  yield takeLatest(types.LOGIN.REQUEST, loginSaga);
}

function* watchLogoutSaga() {
  yield takeLatest(types.LOGOUT.REQUEST, logoutSaga);
}

function* watchBackgroundAuthSaga() {
  yield takeLatest(types.BACKGROUND_AUTHORIZATION, backgroundAuthSaga);
}

export { watchLoginSaga, watchLogoutSaga, watchBackgroundAuthSaga };
