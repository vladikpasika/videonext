import { all } from 'redux-saga/effects';
import {
  watchLoginSaga,
  watchLogoutSaga,
  watchBackgroundAuthSaga,
} from './userSaga';
import {
  watchGetEventLog,
  watchGetCameraAttributes,
  watchGetSets,
  watchGetMediaUrl,
  watchGetCurrentCameraSet,
  watchGetCameraEventLog,
  watchPtzEventRequestSaga,
} from './cameraSet';

export default function* rootSaga() {
  yield all([
    watchLoginSaga(),
    watchBackgroundAuthSaga(),
    watchLogoutSaga(),
    watchGetEventLog(),
    watchGetCameraAttributes(),
    watchGetSets(),
    watchGetMediaUrl(),
    watchGetCurrentCameraSet(),
    watchGetCameraEventLog(),
    watchPtzEventRequestSaga(),
  ]);
}
