import { createStackNavigator, createSwitchNavigator } from 'react-navigation';
import Wrapper from '../components/Wrapper';
import Loader from '../screens/Loader';
import Dashboard from '../screens/Dashboard';
import AddSystem from '../screens/AddSystem';
import SelectSystem from '../screens/SelectSystem';
import CamerasSet from '../screens/CamerasSet';
import Menu from '../screens/Menu';
import EventLog from '../screens/EventLog';
// import Map from '../screens/Map';
import CameraLiveView from '../screens/CameraLiveView';
import SelectSet from '../screens/SelectSet';

const AppNavigator = createStackNavigator(
  {
    Dashboard: { screen: Wrapper(Dashboard) },
    SelectSystem: { screen: Wrapper(SelectSystem) },
    CamerasSet: { screen: Wrapper(CamerasSet) },
    Menu: { screen: Wrapper(Menu) },
    EventLog: { screen: Wrapper(EventLog) },
    // Map: { screen: Wrapper(Map) },
    CameraLiveView: { screen: Wrapper(CameraLiveView) },
    SelectSet: { screen: Wrapper(SelectSet) },
  },
  {
    initialRouteName: 'Dashboard',
    headerMode: 'none',
  }
);

const Auth = createStackNavigator(
  {
    AddSystem: { screen: AddSystem },
  },
  {
    initialRouteName: 'AddSystem',
    headerMode: 'none',
  }
);

const SwitchNavigator = createSwitchNavigator(
  {
    AuthLoading: Loader,
    App: AppNavigator,
    Auth: Auth,
  },
  {
    initialRouteName: 'AuthLoading',
  }
);
export { AppNavigator, SwitchNavigator };
