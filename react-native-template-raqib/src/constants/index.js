const ENDPOINT_LOGIN_INFO = '/api/call/getLoginInfo';
const ENDPOINT_LOGIN = '/api/call/login';
const ENDPOINT_LOGOUT = '/api/call/logout';
const ENDPOINT_GET_OBJECT_LIST = '/api/call/getObjectList';
const ENDPOINT_CAMERA_SNAPSHOT = '/api/snapshot';
const ENDPOINT_EVENT_LOG = '/elog-restful/api/v2/alertlog/eventList';
const ENDPOINT_GET_MEDIA_URL = '/api/call/getMobileMediaURL';
const ENDPOINT_PTZ_SEND_COMMAND = '/api/ptz?';

export {
  ENDPOINT_LOGIN_INFO,
  ENDPOINT_LOGIN,
  ENDPOINT_LOGOUT,
  ENDPOINT_GET_OBJECT_LIST,
  ENDPOINT_CAMERA_SNAPSHOT,
  ENDPOINT_EVENT_LOG,
  ENDPOINT_GET_MEDIA_URL,
  ENDPOINT_PTZ_SEND_COMMAND,
};
