import axios from 'axios';
import axiosCancel from 'axios-cancel';

export const api = (
  apiHost,
  endpoint,
  method,
  data = null,
  requestId = null,
  lastRequestId = null
) => {
  const url = apiHost + endpoint;
  axiosCancel(axios, {
    debug: false,
  });

  if (lastRequestId !== null) {
    axios.cancel(lastRequestId);
  }

  const axiosConfig = {
    method,
    [method.toLowerCase() === 'get' ? 'params' : 'data']: data,
    requestId,
    url,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    validateStatus(status) {
      if (status >= 500) {
        console.log(
          `Server Error ${status} - sorry, something went wrong on the server.`
        );
      }
      return status < 500;
    },
  };
  return axios(axiosConfig)
    .then(res => {
      if (res.status === 200) {
        return Promise.resolve(res);
      } else if (res.status === 401) {
        return Promise.resolve({
          type: 'error',
          message: res.data.error,
        });
      }
      return Promise.reject(res);
    })
    .catch(error => {
      if (error.message === 'Network Error') {
        return Promise.resolve({
          type: 'error',
          message: `${error.message}, invalid server ip or address.`,
        });
      }
    });
};
