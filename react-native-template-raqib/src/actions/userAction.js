export const types = {
  LOGIN_INFO: {
    REQUEST: 'LOGIN_INFO.REQUEST',
    SUCCESS: 'LOGIN_INFO.SUCCESS',
    FAILURE: 'LOGIN_INFO.FAILURE',
  },
  LOGIN: {
    REQUEST: 'LOGIN.REQUEST',
    SUCCESS: 'LOGIN.SUCCESS',
    FAILURE: 'LOGIN.FAILURE',
  },
  CHANGE_MAIN_SYSTEM: 'CHANGE_MAIN_SYSTEM',
  CHANGE_DEVICE_ORIENTATION: 'CHANGE_DEVICE_ORIENTATION',
  BACKGROUND_AUTHORIZATION: 'BACKGROUND_AUTHORIZATION',
  LOGOUT: {
    REQUEST: 'LOGOUT.REQUEST',
    SUCCESS: 'LOGOUT.SUCCESS',
  },
};

const getLoginInfo = apiAddress => ({
  type: types.LOGIN_INFO.REQUEST,
  apiAddress,
});

const changeMainSystem = payload => ({
  type: types.CHANGE_MAIN_SYSTEM,
  payload,
});

const loginInfoSuccess = info => ({
  type: types.LOGIN_INFO.SUCCESS,
  info,
});

const loginInfoFailure = message => ({
  type: types.LOGIN_INFO.FAILURE,
  message,
});

const login = ({
  sysName,
  sysAddress,
  userName,
  password,
  loggedIn = false,
  currentSysAddress = '',
}) => ({
  type: types.LOGIN.REQUEST,
  sysName,
  sysAddress: sysAddress.replace(/\/$/, ''),
  userName,
  password,
  loggedIn,
  currentSysAddress,
});

const loginSuccess = data => ({
  type: types.LOGIN.SUCCESS,
  ...data,
});

const loginFailture = message => ({
  type: types.LOGIN.FAILURE,
  message,
});

const logout = () => ({
  type: types.LOGOUT.REQUEST,
});

const logoutSuccess = responce => ({
  type: types.LOGOUT.SUCCESS,
  responce,
});

const changeDeviceOrientation = data => ({
  type: types.CHANGE_DEVICE_ORIENTATION,
  ...data,
});

const backgroundAuthorization = data => ({
  type: types.BACKGROUND_AUTHORIZATION,
  ...data,
});

export {
  getLoginInfo,
  loginInfoSuccess,
  loginFailture,
  login,
  loginSuccess,
  logout,
  logoutSuccess,
  changeMainSystem,
  changeDeviceOrientation,
  backgroundAuthorization,
  loginInfoFailure,
};
