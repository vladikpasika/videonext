export const types = {
  GET_MEDIA_URL: {
    REQUEST: 'GET_MEDIA_URL.REQUEST',
    SUCCESS: 'GET_MEDIA_URL.SUCCESS',
    FAILURE: 'GET_MEDIA_URL.FAILURE',
  },
  GET_SETS: {
    REQUEST: 'GET_SETS.REQUEST',
    SUCCESS: 'GET_SETS.SUCCESS',
    FAILURE: 'GET_SETS.FAILURE',
  },
  GET_EVENT_LOG: {
    REQUEST: 'GET_EVENT_LOG.REQUEST',
    SUCCESS: 'GET_EVENT_LOG.SUCCESS',
    FAILURE: 'GET_EVENT_LOG.FAILURE',
    CLEAR: 'GET_EVENT_LOG.CLEAR',
  },
  GET_CAMERA_EVENT_LOG: {
    REQUEST: 'GET_CAMERA_EVENT_LOG.REQUEST',
    SUCCESS: 'GET_CAMERA_EVENT_LOG.SUCCESS',
    FAILURE: 'GET_CAMERA_EVENT_LOG.FAILURE',
  },
  GET_CAMERA_ATTRIBUTES: {
    REQUEST: 'GET_CAMERA_ATTRIBUTES.REQUEST',
    SUCCESS: 'GET_CAMERA_ATTRIBUTES.SUCCESS',
    FAILURE: 'GET_CAMERA_ATTRIBUTES.FAILURE',
  },
  GET_CURRENT_CAMERA_SET: {
    REQUEST: 'GET_CURRENT_CAMERA_SET.REQUEST',
    SUCCESS: 'GET_CURRENT_CAMERA_SET.SUCCESS',
    FAILURE: 'GET_CURRENT_CAMERA_SET.FAILURE',
  },
  UPDATE_LOGS_BY_INTERVAL: {
    REQUEST: 'UPDATE_LOGS_BY_INTERVAL.REQUEST',
    SUCCESS: 'UPDATE_LOGS_BY_INTERVAL.SUCCESS',
    FAILURE: 'UPDATE_LOGS_BY_INTERVAL.FAILURE',
    CANCEL: 'UPDATE_LOGS_BY_INTERVAL.CANCEL',
  },
  PTZ_EVENT: {
    REQUEST: 'PTZ_EVENT.REQUEST',
    SUCCESS: 'PTZ_EVENT.SUCCESS',
    FAILURE: 'PTZ_EVENT.FAILURE',
    SAVE_REQUEST_ID: 'PTZ_EVENT.SAVE_REQUEST_ID',
  },
  RESET_CAMERAS: 'RESET_CAMERAS',
  SET_CURRENT_SET: 'SET_CURRENT_SET',
  SET_CURRENT_CAMERA: 'SET_CURRENT_CAMERA',
  SET_CURRENT_CAMERA_TIME: 'SET_CURRENT_CAMERA_TIME',
};

//EventLog cancel
export const startUpdateLogsByInterval = () => ({
  type: types.UPDATE_LOGS_BY_INTERVAL.REQUEST,
});

export const cancelUpdateLogsByInterval = () => ({
  type: types.UPDATE_LOGS_BY_INTERVAL.CANCEL,
});

//EventLog requst flow
export const getEventLog = (payload, meta = '') => ({
  type: types.GET_EVENT_LOG.REQUEST,
  payload,
  meta,
});

export const successEventLog = (payload, meta = '') => ({
  type: types.GET_EVENT_LOG.SUCCESS,
  payload,
  meta,
});

export const failureEventLog = () => ({
  type: types.GET_EVENT_LOG.FAILURE,
});

export const clearEventLog = () => ({
  type: types.GET_EVENT_LOG.CLEAR,
});

export const getCameraEventLog = (payload, meta = '') => ({
  type: types.GET_CAMERA_EVENT_LOG.REQUEST,
  payload,
  meta,
});

export const successCameraEventLog = (payload, meta = '') => ({
  type: types.GET_CAMERA_EVENT_LOG.SUCCESS,
  payload,
  meta,
});

export const failureCameraEventLog = () => ({
  type: types.GET_CAMERA_EVENT_LOG.FAILURE,
});

//CamerasSet request flow
export const getCurrentCameraSet = payload => ({
  type: types.GET_CURRENT_CAMERA_SET.REQUEST,
  payload,
});

export const successCurrentCameraSet = payload => ({
  type: types.GET_CURRENT_CAMERA_SET.SUCCESS,
  payload,
});

export const failureCurrentCameraSet = () => ({
  type: types.GET_CURRENT_CAMERA_SET.FAILURE,
});

export const setCurrentCamera = payload => ({
  type: types.SET_CURRENT_CAMERA,
  payload,
});

export const setCurrentCameraTime = payload => ({
  type: types.SET_CURRENT_CAMERA_TIME,
  payload,
});

export const getCameraAttributes = () => ({
  type: types.GET_CAMERA_ATTRIBUTES.REQUEST,
});

export const successCameraAttributes = payload => ({
  type: types.GET_CAMERA_ATTRIBUTES.SUCCESS,
  payload,
});

export const failtureCameraAttributes = () => ({
  type: types.GET_CAMERA_ATTRIBUTES.FAILURE,
});

export const getSets = () => ({
  type: types.GET_SETS.REQUEST,
});

export const successSets = payload => ({
  type: types.GET_SETS.SUCCESS,
  payload,
});

export const failtureSets = () => ({
  type: types.GET_SETS.FAILURE,
});

export const getMediaUrl = () => ({
  type: types.GET_MEDIA_URL.REQUEST,
});

export const getMediaUrlSuccess = payload => ({
  type: types.GET_MEDIA_URL.SUCCESS,
  payload,
});

export const getMediaUrlFailture = () => ({
  type: types.GET_MEDIA_URL.FAILURE,
});

export const ptzEventRequest = payload => ({
  type: types.PTZ_EVENT.REQUEST,
  ...payload,
});

export const ptzEventSaveRequestId = payload => ({
  type: types.PTZ_EVENT.SAVE_REQUEST_ID,
  ...payload,
});

export const setCurrentSet = payload => ({
  type: types.SET_CURRENT_SET,
  payload,
});

export const resetCameras = () => ({ type: types.RESET_CAMERAS });
