import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container } from './styled';

class Redirect extends Component {

  componentDidMount() {
    this.props.navigation.navigate(this.props.navigation.state.params.redirectTo);
  }

  render() {
    return <Container color="#9c9c9b" />;
  }
}

Redirect.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Redirect;
