import React, { PureComponent } from 'react';
import {
  StyleSheet,
  StatusBar,
  View,
  TouchableOpacity,
  TouchableHighlight,
  Text,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import IconAwesome from 'react-native-vector-icons/FontAwesome';

import { Col, Grid, Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Feather';
import { Container } from './styled';
import Header from '../components/Header';
import Spinner from '../components/spinner';
import Player from '../components/player';
import EventLogsTable from '../components/EventLogsTable';
import PTZ from '../components/player/ptz';
import { ENDPOINT_CAMERA_SNAPSHOT } from '../constants';
import { normalize, getDimensions } from '../utils/helpers';

import {
  getMediaUrl,
  getCameraEventLog,
  setCurrentCamera,
  setCurrentCameraTime,
  clearEventLog,
  ptzEventRequest,
} from '../actions/cameras';
import Slider from '../components/player/slider';
import Joystick from '../utils/Joystick';

const { width, height } = getDimensions();

const __ = string => {
  return string;
};

class CameraLiveView extends PureComponent {
  constructor(props) {
    super(props);
    Joystick.init();
    Joystick.setObj(this.props.currentCamera);
    this.state = {
      currentLog: 0,
      cameraEventLogPage: 0,
      playerKey: Date.now(),
      liveRecord: true,
      showCalendar: false,
      isVisible: false,
      showPtz: false,
    };
  }

  componentDidMount() {
    if (this.props.navigation.getParam('isArchive', false)) {
      this.setState({ liveRecord: false });
    }
    this.props.getCameraEventLog(
      {
        size: 20,
        page: 0,
        sort: 'WHEN',
        sortasc: false,
      },
      'loadLogs'
    );
    this.props.getMediaUrl();
    this.eventLogTimer = this.tickTimer(() => {
      if (Object.keys(this.props.eventLogs).length) {
        this.props.getCameraEventLog(
          {
            size: Object.keys(this.props.eventLogs).length,
            page: 0,
            sort: 'WHEN',
            sortasc: false,
          },
          'updateLogs'
        );
      }
    });
  }

  componentDidUpdate(PrevProps, prevState) {
    const { cameraEventLogPage } = this.state;
    if (PrevProps.currentCameraTime !== this.props.currentCameraTime) {
      this.props.getMediaUrl();
    } else if (prevState.currentLog !== this.state.currentLog) {
      this.setCurrentCameraEventLog(this.state.currentLog);
    } else if (
      prevState.cameraEventLogPage !== cameraEventLogPage &&
      cameraEventLogPage !== 0
    ) {
      this.props.getCameraEventLog(
        {
          size: 20,
          page: this.state.cameraEventLogPage,
          sort: 'WHEN',
          sortasc: false,
        },
        'updateLogs'
      );
    }
  }

  componentWillUnmount() {
    clearInterval(this.eventLogTimer);
  }

  tickTimer = func => setInterval(func, 20000);

  logPressHandler = id => {
    this.setState({
      currentLog: id,
      playerKey: Date.now(),
    });
  };

  reloadPlayer = () => {
    this.setState({
      playerKey: Date.now(),
    });
  };

  setCurrentCameraEventLog = id => {
    const { eventLogs } = this.props;
    const currentLogObj = eventLogs[id];
    this.props.setCurrentCameraTime({
      startTime: currentLogObj.from / 1000,
      endTime: currentLogObj.to / 1000,
    });
    this.setState({ liveRecord: false });
  };

  getArchiveMediaUrl = (startTime, endTime) => {
    this.props.setCurrentCameraTime({
      startTime,
      endTime,
    });
    this.props.getMediaUrl();
    this.setState({ liveRecord: false });
  };

  setFullScreen = (show, width, height) => {
    this.props.changeDeviceOrientation({
      isLandscape: show,
      width: width,
      height: height,
    });
  };

  onFlatListEndReached = () => {
    if (!this.props.loading) {
      this.setState({ cameraEventLogPage: this.state.cameraEventLogPage + 1 });
    }
  };

  setCalendar = value => {
    this.setState({ showCalendar: value });
  };

  handlePtzPress = command => {
    this.props.ptzEventRequest({ command });
  };

  isVisible = value => {
    this.setState({ isVisible: value });
  };

  ptzIconPressHandler = value => {
    this.setState({ showPtz: value });
  };

  getZoomValue = value => {
    const srvCommand = Joystick.onAxis('3', value * 0.2);
    srvCommand.then(command => {
      this.handlePtzPress(command);
    });
  };

  handlePtzPressBtn = (position, command) => {
    if (!command) {
      return;
    }
    const srvCommand = Joystick.sendCommand(command[0]);
    srvCommand.then(command => {
      this.handlePtzPress(command);
    });
  };

  renderControllItem(position, isSmall, command = null) {
    const { isLandscape } = this.props;
    let style;
    if (isLandscape) {
      style = styles.iconSmallerContainer;
    } else {
      style = isSmall ? styles.iconSmallerContainer : styles.iconContainer;
    }
    return (
      <TouchableHighlight
        style={style}
        onPress={() => this.handlePtzPressBtn(position, command)}
      >
        <Icon
          name={position}
          style={isSmall ? styles.iconSmaller : styles.icon}
        />
      </TouchableHighlight>
    );
  }

  renderPtzZoomLandscape = () => {
    return (
      <Grid
        style={[
          styles.zoom,
          {
            height: '100%',
            width: normalize(50),
          },
        ]}
      >
        <Col size={1}>
          <Row size={5} style={{ backgroundColor: 'transparent' }}>
            <Slider getZoomValue={this.getZoomValue} />
          </Row>
        </Col>
      </Grid>
    );
  };

  renderJoystickLandscape = () => {
    return (
      <View
        style={[
          styles.gridJoystick,
          {
            height: '50%',
            width: '30%',
          },
        ]}
      >
        <Grid>
          <Col>
            <Row style={styles.row} />
            <Row style={styles.row}>
              {this.renderControllItem('arrow-left', false, [
                'mode=step&move=left',
              ])}
            </Row>
            <Row style={styles.row} />
          </Col>
          <Col>
            <Row style={styles.row}>
              {this.renderControllItem('arrow-up', false, [
                'mode=step&move=up',
              ])}
            </Row>
            <Row style={styles.row}>{this.renderControllItem('circle')}</Row>
            <Row style={styles.row}>
              {this.renderControllItem('arrow-down', false, [
                'mode=step&move=down',
              ])}
            </Row>
          </Col>
          <Col>
            <Row style={styles.row} />
            <Row style={styles.row}>
              {this.renderControllItem('arrow-right', false, [
                'mode=step&move=right',
              ])}
            </Row>
            <Row style={styles.row} />
          </Col>
        </Grid>
      </View>
    );
  };

  render() {
    const {
      currentCamera,
      currentCameraTime,
      loading,
      playerUri,
      eventLogs,
      navigation,
      deviceWidth,
      deviceHeight,
      serverAddress,
      mediaUrlLoading,
      isLandscape,
      listCameras,
      serverName,
    } = this.props;
    const eventLogKeys = Object.keys(eventLogs);
    const isPTZ =
      listCameras[currentCamera].attributes.POSITIONCTL &&
      listCameras[currentCamera].attributes.POSITIONCTL !== 'none';
    return (
      <Container color="#ffffff" style={{ position: 'relative' }}>
        <StatusBar hidden={isLandscape} />
        <Header
          navigation={this.props.navigation}
          visible={!isLandscape}
          leftTitle={__(serverName)}
          leftIcon={true}
          centerTitle={
            (currentCamera && listCameras[currentCamera].name) ||
            __('Video View')
          }
          showIconMenu
        />
        <Spinner show={loading} />
        <Player
          playerUri={playerUri}
          ptz={listCameras[currentCamera].attributes.POSITIONCTL}
          ptzIconPressHandler={this.ptzIconPressHandler}
          isVisible={this.isVisible}
          handlePtzPress={this.handlePtzPress}
          eLogsLength={eventLogKeys.length}
          eLogsKeys={eventLogKeys}
          currentCamera={currentCamera}
          currentCameraTime={currentCameraTime}
          logPressHandler={this.logPressHandler}
          loading={loading}
          reloadPlayer={this.reloadPlayer}
          playerKey={this.state.playerKey}
          navigation={navigation}
          currentLog={+this.state.currentLog}
          image={`${serverAddress}${ENDPOINT_CAMERA_SNAPSHOT}?&objid=${currentCamera}`}
          mediaUrlLoading={mediaUrlLoading}
          deviceWidth={deviceWidth}
          deviceHeight={deviceHeight}
          setFullScreen={this.setFullScreen}
          getArchiveMediaUrl={this.getArchiveMediaUrl}
          isLandscape={isLandscape}
          getMediaUrl={this.props.getMediaUrl}
          showCalendar={this.state.showCalendar}
          setCalendar={this.setCalendar}
          liveRecord={this.state.liveRecord}
          archiveIndicator={
            <IconAwesome
              name="circle"
              // onPress={() => {
              //   if (!this.state.liveRecord) {
              //     this.props.setCurrentCameraTime({});
              //     this.setState({ liveRecord: true });
              //   }
              // }}
              style={[
                styles.liveCircleIndicator,
                { color: this.state.liveRecord ? '#6cbd7d' : '#f44842' },
              ]}
            />
          }
          archiveSwitch={
            <TouchableOpacity
              onPress={() => {
                if (!this.state.liveRecord) {
                  this.props.setCurrentCameraTime({});
                  this.setState({ liveRecord: true });
                } else {
                  this.setState({ showCalendar: true });
                }
              }}
              hitSlop={{ top: 25, bottom: 15, left: 15, right: 15 }}
              style={styles.archiveSwitch}
            >
              <Text style={styles.archiveIcon}>
                {this.state.liveRecord ? 'A' : 'L'}
              </Text>
            </TouchableOpacity>
          }
        />
        {!isLandscape ? (
          isPTZ ? (
            <View style={styles.secondPortraitContainer}>
              <PTZ
                currentCamera={currentCamera}
                handlePtzPress={this.handlePtzPress}
                isVisible={this.state.isVisible}
                liveRecord={this.state.liveRecord}
                ptz={listCameras[currentCamera].attributes.POSITIONCTL}
                showPtz={this.state.showPtz}
              />
            </View>
          ) : (
            <View style={styles.secondPortraitContainer}>
              <EventLogsTable
                eventsLogs={eventLogs}
                deviceWidth={
                  deviceHeight < deviceWidth ? deviceHeight : deviceWidth
                }
                navigation={navigation}
                logPressHandler={this.logPressHandler}
                listCameras={listCameras}
                currentCameraTime={currentCameraTime}
                onFlatListEndReached={this.onFlatListEndReached}
              />
            </View>
          )
        ) : (
          isPTZ && (
            <>
              {this.renderPtzZoomLandscape()}
              {this.renderJoystickLandscape()}
            </>
          )
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  playerContainer: {
    flex: 1,
    backgroundColor: '#000000',
    position: 'absolute',
    top: 0,
  },
  zoom: {
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  gridJoystick: {
    position: 'absolute',
    top: normalize(90),
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  eventLogsLandscapeContainer: {
    borderColor: '#000000',
    flex: 1,
    position: 'absolute',
    width: width,
    height: height * 0.4,
    right: 0,
    top: 0,
    borderTopWidth: 0,
  },
  secondPortraitContainer: {
    borderColor: '#000000',
    flex: 2,
    position: 'relative',
    width: width,
    height: height * 0.4,
    right: 0,
    top: 0,
    borderTopWidth: 1,
  },
  liveCircleIndicator: {
    fontSize: 21,
    paddingTop: 10,
    flex: 1,
    zIndex: 1000,
    position: 'absolute',
    bottom: 10,
    right: 10,
    backgroundColor: 'transparent',
  },
  archiveIcon: {
    color: 'white',
    fontSize: 23,
  },
  archiveSwitch: {
    zIndex: 1000,
    position: 'absolute',
    bottom: 10,
    left: 10,
    backgroundColor: 'transparent',
  },
  iconContainer: {
    backgroundColor: '#501f6e',
    padding: 20,
  },
  icon: {
    fontSize: 25,
    color: 'white',
  },
  iconSmallerContainer: {
    backgroundColor: '#501f6e',
    padding: 10,
  },
  iconSmaller: {
    fontSize: 15,
    color: 'white',
  },
  row: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

CameraLiveView.propTypes = {
  navigation: PropTypes.object.isRequired,
  playerUri: PropTypes.string.isRequired,
  currentCamera: PropTypes.string,
  currentCameraTime: PropTypes.object,
  listCameras: PropTypes.object.isRequired,
  getMediaUrl: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  getCameraEventLog: PropTypes.func.isRequired,
  eventLogs: PropTypes.object.isRequired,
  setCurrentCamera: PropTypes.func.isRequired,
  setCurrentCameraTime: PropTypes.func.isRequired,
  serverAddress: PropTypes.string.isRequired,
  mediaUrlLoading: PropTypes.bool.isRequired,
  deviceWidth: PropTypes.number.isRequired,
  deviceHeight: PropTypes.number.isRequired,
  isLandscape: PropTypes.bool.isRequired,
  clearEventLog: PropTypes.func.isRequired,
  ptzEventRequest: PropTypes.func.isRequired,
  changeDeviceOrientation: PropTypes.func.isRequired,
};

const mapStateToProps = ({
  cameras: {
    playerUri,
    currentCamera,
    currentCameraTime,
    list,
    loading,
    eventLogs,
    mediaUrlLoading,
  },
  user: { deviceWidth, deviceHeight, serverAddress, isLandscape, serverName },
}) => ({
  playerUri,
  currentCamera,
  currentCameraTime,
  listCameras: list,
  loading,
  deviceWidth,
  deviceHeight,
  isLandscape,
  eventLogs,
  serverAddress,
  mediaUrlLoading,
  serverName,
});

export default connect(
  mapStateToProps,
  {
    getMediaUrl,
    getCameraEventLog,
    setCurrentCamera,
    setCurrentCameraTime,
    clearEventLog,
    ptzEventRequest,
  }
)(CameraLiveView);
