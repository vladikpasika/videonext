import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FlatList, AppState, Dimensions } from 'react-native';

import Header from '../components/Header';
import CameraListItem from '../components/cameraListItem';
import { setCurrentCamera } from '../actions/cameras';
import { ENDPOINT_CAMERA_SNAPSHOT } from '../constants';
import CONFIG from 'react-native-config';
//Styled
import { Container, Content } from './styled';

const { width, height } = Dimensions.get('window');
const __ = string => {
  return string;
};

class CamerasSet extends PureComponent {
  constructor(props) {
    super(props);
    this.viewabilityConfig = { viewAreaCoveragePercentThreshold: 50 };
    this.state = {
      viewable: {},
    };
    this.ticks = {};
    this.flatOrientation = {
      landscape: Math.random(),
      notLandscape: Math.random(),
    };
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    if (Object.keys(this.props.cameraList).length !== 0) {
      this.startCameraReload();
    }
  }

  componentDidUpdate(prevProps) {
    if (
      Object.keys(prevProps.cameraList).length === 0 &&
      Object.keys(this.props.cameraList).length !== 0
    ) {
      console.log('did update');
      this.startCameraReload();
    }
  }
  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
    Object.keys(this.ticks).forEach(key => clearInterval(this.ticks[key]));
  }

  tickTimer = func =>
    setInterval(func, Number.parseInt(CONFIG.CAMERA_SET_TICK, 10));

  onViewableItemsChanged = info => {
    info.changed.forEach(item => {
      if (!this.state.viewable[item.key] && item.isViewable) {
        this.ticks[item.key] = this.tickTimer(() =>
          this.setState({
            viewable: { ...this.state.viewable, [item.key]: Date.now() },
          })
        );
      } else if (this.state.viewable[item.key] && !item.isViewable) {
        clearInterval(this.ticks[item.key]);
        this.setState({
          viewable: { ...this.state.viewable, [item.key]: false },
        });
      }
    });
  };

  onFlatItemPress = id => {
    this.props.setCurrentCamera(id);
    this.props.navigation.navigate('CameraLiveView');
  };

  titlePressHandler = () => {
    this.props.navigation.navigate('SelectSet');
  };

  _handleAppStateChange = nextAppState => {
    if (
      nextAppState === 'active' &&
      Object.keys(this.props.cameraList).length !== 0
    ) {
      Object.keys(this.state.viewable).forEach(item => {
        this.ticks[item] = this.tickTimer(() =>
          this.setState({
            viewable: { ...this.state.viewable, [item]: Date.now() },
          })
        );
      });
    } else if (nextAppState === 'inactive') {
      Object.keys(this.ticks).forEach(key => clearInterval(this.ticks[key]));
    }
  };

  startCameraReload = () => {
    this.setState({
      viewable: Object.keys(this.props.cameraList).reduce(
        (prevItem, item) => ({ ...prevItem, [item]: false }),
        {}
      ),
    });
  };

  render() {
    const {
      currentSet,
      sets,
      isLandscape,
      serverAddress,
      serverName,
    } = this.props;
    const screenTitle =
      sets[currentSet] !== undefined ? sets[currentSet].name : 'Cameras';
    const deviceWidth = isLandscape ? height * 0.8 : width;

    return (
      <Container color="#ffffff">
        <Header
          leftTitle={__(serverName)} //
          leftIcon={true}
          centerTitle={__('Set: ') + screenTitle}
          showIconMenu
          navigation={this.props.navigation}
        />
        <Content>
          <FlatList
            key={
              this.props.isLandscape
                ? this.flatOrientation.landscape
                : this.flatOrientation.notLandscape
            }
            style={{
              flex: 1,
              width: deviceWidth,
            }}
            data={Object.keys(this.props.cameraList)}
            numColumns={this.props.isLandscape ? 5 : 3}
            onViewableItemsChanged={this.onViewableItemsChanged}
            viewabilityConfig={this.viewabilityConfig}
            keyExtractor={key => key}
            renderItem={({ item: key }) => (
              <CameraListItem
                id={this.props.cameraList[key].obj}
                image={`${serverAddress}${ENDPOINT_CAMERA_SNAPSHOT}?date=${this
                  .state.viewable[key] || 0}&objid=${key}&downscale=`}
                onPressItem={this.onFlatItemPress}
                text={this.props.cameraList[key].name}
                isLandscape={isLandscape}
                width={deviceWidth}
              />
            )}
          />
        </Content>
      </Container>
    );
  }
}

CamerasSet.propTypes = {
  navigation: PropTypes.object.isRequired,
  cameraList: PropTypes.object.isRequired,
  setCurrentCamera: PropTypes.func.isRequired,
  isLandscape: PropTypes.bool.isRequired,
  currentSet: PropTypes.string.isRequired,
  sets: PropTypes.object.isRequired,
};

const mapStateToProps = ({
  user: { isLandscape, serverAddress, serverName },
  cameras: { list: cameraList, currentSet, sets },
}) => ({
  isLandscape,
  cameraList,
  currentSet,
  sets,
  serverName,
  serverAddress,
});

export default connect(
  mapStateToProps,
  { setCurrentCamera }
)(CamerasSet);
