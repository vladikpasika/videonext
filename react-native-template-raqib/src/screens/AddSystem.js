import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  Keyboard,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import Orientation from 'react-native-orientation';
import { Button } from 'react-native-elements';
import PropTypes from 'prop-types';
import CONFIG from 'react-native-config';

import { login } from '../actions/userAction';
import Header from '../components/Header';
import FormInput from '../components/FormInput';
import Spinner from '../components/spinner';
import { KeyboardAvoidingContainer } from './styled';
import { getDimensions, normalize } from '../utils/helpers';
import logoImg from '../assets/images/smallLogo.png';

const { width, height } = getDimensions();
const __ = string => {
  return string;
};
class AddSystem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      systemName: props.systemName || '',
      serverAddress: props.serverAddress || '',
      userName: '',
      password: '',
      showKeyboard: false,
    };
  }

  componentDidMount() {
    Orientation.lockToPortrait();
  }

  async UNSAFE_componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.keyboardDidHide
    );
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.user.loggedIn !== prevProps.user.loggedIn &&
      this.props.user.loggedIn
    ) {
      this.props.navigation.navigate('AuthLoading');
    }

    if (prevProps.user.error === null && this.props.user.error !== null) {
      alert(this.props.user.error);
    }
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    Orientation.unlockAllOrientations();
  }

  keyboardDidShow = () => {
    this.setState({ showKeyboard: true });
  };

  keyboardDidHide = () => {
    this.setState({ showKeyboard: false });
  };

  updateState = (name, value) => {
    const newState = {};
    newState[name] = value;
    this.setState(newState);
  };

  pressDoneHandler = () => {
    const { login, user, navigation } = this.props;
    let { systemName, serverAddress, userName, password } = this.state;
    if (
      (serverAddress !== '' && userName !== '' && password !== '',
      systemName !== '')
    ) {
      serverAddress = serverAddress.replace(/^http:\/\//i, 'https://');
      if (!serverAddress.match('https://')) {
        serverAddress = `https://${serverAddress}`;
      }
      login({
        sysName: systemName,
        sysAddress: serverAddress,
        userName,
        password,
        loggedIn: user.loggedIn,
        currentSysAddress: user.serverAddress,
      });
      if (user.loggedIn) {
        navigation.navigate('SelectSystem');
      }
    } else {
      alert('Please, fill all required fields.');
    }
  };

  render() {
    const { loading, serverName } = this.props.user;
    const {
      userName,
      password,
      showKeyboard,
      systemName,
      serverAddress,
    } = this.state;

    return (
      <KeyboardAvoidingContainer behavior="height" color="#fff">
        <Header
          leftTitle={__(serverName || CONFIG.SERVER_NAME)}
          navigation={this.props.navigation}
        />
        <View styles={{ flex: 1, backgroundColor: '#fff' }}>
          <ScrollView keyboardShouldPersistTaps={'handled'}>
            <Spinner show={loading} />
            <View
              style={{
                paddingTop: showKeyboard ? 0 : height * 0.01,
                backgroundColor: '#fff',
              }}
            >
              <View style={styles.imageContainer}>
                <Image
                  source={logoImg}
                  style={showKeyboard ? styles.smallImage : styles.image}
                />
              </View>
              <View style={styles.form}>
                <FormInput
                  updateState={this.updateState}
                  placeHolderText="SYSTEM NAME"
                  stateName="systemName"
                  labelText={__('System name')}
                  value={systemName}
                  required={false}
                  secureText={false}
                />
                <FormInput
                  updateState={this.updateState}
                  placeHolderText="SERVER IP OR URL ADDRESS"
                  stateName="serverAddress"
                  labelText={__('Server Address')}
                  value={serverAddress}
                  required={true}
                  secureText={false}
                />
                <FormInput
                  updateState={this.updateState}
                  labelText={__('Username')}
                  stateName="userName"
                  value={userName}
                  secureText={false}
                />
                <FormInput
                  updateState={this.updateState}
                  labelText={__('Password')}
                  stateName="password"
                  value={password}
                  secureText={true}
                />
                <Button
                  disabled={loading}
                  title={__('LOGIN')}
                  onPress={() => this.pressDoneHandler()}
                  buttonStyle={styles.btnDone}
                  fontSize={20}
                  textStyle={styles.btnDoneTitle}
                />
              </View>
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingContainer>
    );
  }
}

const styles = StyleSheet.create({
  imageContainer: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 150,
    height: 150,
  },
  smallImage: {
    marginTop: 10,
    width: 70,
    height: 70,
  },
  form: {
    flex: 1,
    alignItems: 'center',
  },
  btnDone: {
    marginTop: 40,
    backgroundColor: '#cc3366',
    borderRadius: 2,
    width: width * 0.46,
    height: normalize(50),
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.5,
    elevation: 3,
  },
  btnDoneTitle: {
    ...Platform.select({
      ios: {
        fontFamily: 'STC',
        fontWeight: '500',
        fontSize: 18,
      },
      android: {
        fontFamily: 'STC-Bold',
        fontSize: 20,
      },
    }),
  },
});

const mapStateToProps = state => ({
  user: state.user,
  systemName: CONFIG.SERVER_NAME,
  serverAddress: CONFIG.SERVER_ADDRESS,
});

AddSystem.propTypes = {
  user: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  login: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  { login }
)(AddSystem);
