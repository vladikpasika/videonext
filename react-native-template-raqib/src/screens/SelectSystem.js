import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FlatList, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';

import Header from '../components/Header';
import ListItem from '../components/listItem';
import { getDimensions } from '../utils/helpers';
//Styled
import { Content, Container } from './styled';
//Actions
import { changeMainSystem, login } from '../actions/userAction';

const { width } = getDimensions();

class SelectSystem extends Component {
  constructor(props) {
    super(props);
    this.viewabilityConfig = {
      minimumViewTime: 500,
      viewAreaCoveragePercentThreshold: 60,
    };
  }

  toggleBookMark = id => {
    const {
      systems,
      changeMainSystem,
      loggedIn,
      serverAddress,
      login,
    } = this.props;

    login({
      sysName: systems[id].sysName,
      sysAddress: systems[id].sysAddress,
      userName: systems[id].userName,
      password: systems[id].password,
      loggedIn,
      currentSysAddress: serverAddress,
    });
    changeMainSystem(id);
    this.props.navigation.navigate('Dashboard');
  };

  render() {
    return (
      <Container color="#9c9c9b">
        <Header
          navigation={this.props.navigation}
          title="Select System"
          showIcons={true}
          showLeftIcon
          leftIcon
        />
        <Content>
          <FlatList
            keyExtractor={key => key}
            style={styles.flatList}
            data={Object.keys(this.props.systems)}
            renderItem={({ item: key }) => (
              <ListItem
                onIconPress={this.toggleBookMark}
                onPressItem={this.toggleBookMark}
                id={key}
                marked={key === this.props.mainSystem}
                title={this.props.systems[key].sysName || 'Empty Name'}
                subTitle={this.props.systems[key].sysAddress}
              />
            )}
          />
          <Button
            title="Add System"
            onPress={() => this.props.navigation.navigate('AddSystem')}
            buttonStyle={styles.btnDone}
            fontWeight="800"
            fontSize={18}
          />
        </Content>
      </Container>
    );
  }
}

SelectSystem.propTypes = {
  systems: PropTypes.object.isRequired,
  navigation: PropTypes.object,
  changeMainSystem: PropTypes.func.isRequired,
  mainSystem: PropTypes.string.isRequired,
  login: PropTypes.func.isRequired,
  serverAddress: PropTypes.string.isRequired,
  loggedIn: PropTypes.bool.isRequired,
};

SelectSystem.defaultProps = {
  systems: {
    ['associate1']: {
      sysName: 'System Name',
      sysAddress: 'website url',
    },
    ['associate2']: {
      sysName: 'System Name',
      sysAddress: 'website url',
    },
  },
};

const styles = StyleSheet.create({
  btnDone: {
    backgroundColor: 'black',
    width: width * 0.95,
    borderRadius: 4,
    height: 48,
    marginBottom: 10,
  },
  flatList: {
    paddingBottom: 20,
  },
});

const mapStateToProps = ({
  user: { systems, mainSystem, loggedIn, serverAddress },
}) => ({
  systems,
  mainSystem,
  loggedIn,
  serverAddress,
});

export default connect(
  mapStateToProps,
  { changeMainSystem, login }
)(SelectSystem);
