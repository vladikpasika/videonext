import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { backgroundAuthorization } from '../actions/userAction';
import Splash from '../components/Splash';
import { getDimensions } from '../utils/helpers';

const { width, height } = getDimensions();
const __ = string => {
  return string;
};

class Loader extends Component {
  componentDidMount() {
    const { loggedIn } = this.props.user;
    if (loggedIn === false) {
      this.props.navigation.navigate('Auth');
    } else {
      this.backgroundAuth();
      this.props.navigation.navigate('App');
    }
  }

  backgroundAuth = () => {
    const { serverAddress, userName, password } = this.props.user;
    this.props.backgroundAuthorization({
      serverAddress,
      userName,
      password,
      width,
      height,
    });
  };

  render() {
    return (
      <Splash
        centerMessageBox={false}
        messageText={__(
          `Welcome to ${this.props.serverName}\nLoading. Please, wait...`
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  serverName: state.user.serverName,
});

Loader.propTypes = {
  user: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  backgroundAuthorization: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  { backgroundAuthorization }
)(Loader);
