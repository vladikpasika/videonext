import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Header from '../components/Header';
import { Container } from './styled';
import { getDimensions } from '../utils/helpers';

const imgCameras = require('../assets/images/cameras.png');
const imgCEventLog = require('../assets/images/eventLog.png');

const { width } = getDimensions();
const __ = string => {
  return string;
};

class Dashboard extends Component {
  itemsPressHandler = route => {
    const { currentSet } = this.props;
    const nameScreen =
      currentSet !== null && currentSet.length > 0 ? route : 'SelectSet';
    this.props.navigation.navigate(nameScreen, { nextRoute: route });
  };

  render() {
    const { navigation, serverName } = this.props;

    return (
      <Container color="#ffffff">
        <Header
          navigation={navigation}
          leftTitle={__(serverName)}
          showIconMenu={true}
        />
        <View style={[styles.contentContainer]}>
          <TouchableOpacity
            style={styles.row}
            onPress={() => this.itemsPressHandler('CamerasSet')}
          >
            <View style={styles.iconContainer}>
              <Image
                resizeMode="contain"
                style={styles.icon}
                source={imgCameras}
              />
            </View>
            <View style={styles.textContainer}>
              <Text style={styles.title}>{__('Cameras')}</Text>
              <Text style={styles.description}>
                {__('Matrix with accessible camera\nsnapshots refreshed often')}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.row}
            onPress={() => this.itemsPressHandler('EventLog')}
          >
            <View style={styles.iconContainer}>
              <Image
                resizeMode="contain"
                style={styles.icon}
                source={imgCEventLog}
              />
            </View>
            <View style={styles.textContainer}>
              <Text style={styles.title}>{__('Event Log')}</Text>
              <Text style={styles.description}>
                {__('List of events and alerts,\nrefreshed often')}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    backgroundColor: '#ffffff',
    marginTop: 10,
  },
  row: {
    width,
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconContainer: {
    width: width * 0.3,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: width * 0.3,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainer: {
    width: width * 0.7,
    height: 100,
  },
  title: {
    color: '#501f6e',
    paddingVertical: 10,
    fontSize: 21,
    ...Platform.select({
      ios: {
        fontFamily: 'STC',
      },
      android: {
        fontFamily: 'STC-Regular',
      },
    }),
  },
  description: {
    color: '#999999',
    fontSize: 16,
    ...Platform.select({
      ios: {
        fontFamily: 'STC',
        fontWeight: '500',
      },
      android: {
        fontFamily: 'STC-Bold',
      },
    }),
    lineHeight: 20,
  },
});

const mapStateToProps = state => ({
  user: state.user,
  currentSet: state.cameras.currentSet,
  serverName: state.user.serverName,
});

Dashboard.propTypes = {
  user: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  currentSet: PropTypes.string.isRequired,
};

export default connect(
  mapStateToProps,
  null
)(Dashboard);
