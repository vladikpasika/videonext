import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  FlatList,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Platform,
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';

import Header from '../components/Header';
import Spinner from '../components/spinner';
import { getDimensions } from '../utils/helpers';
import { Container } from './styled';
import {
  getSets,
  getCurrentCameraSet,
  setCurrentSet,
} from '../actions/cameras';

const { width } = getDimensions();
const __ = string => {
  return string;
};

class SelectSet extends Component {
  componentDidMount() {
    this.props.getSets();
  }

  onPressHandler = id => {
    const nextRoute = this.props.navigation.getParam('nextRoute', 'CamerasSet');
    this.props.setCurrentSet(id);
    if (nextRoute === 'EventLog') {
      return this.resetNavigationParamsAndRedirect(); // reset eventlogs param, these params can causes bug if we first time go to the log screen and select the set
    }
    this.props.getCurrentCameraSet(id);
    this.props.navigation.navigate(nextRoute);
  };

  resetNavigationParamsAndRedirect() {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'EventLog' })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    const { loading, sets, serverName } = this.props;
    return (
      <Container color="#ffffff">
        <Header
          leftTitle={__(serverName)}
          leftIcon={true}
          centerTitle={__('Select Set')}
          centerTitleSize={21}
          showIconMenu={false}
          navigation={this.props.navigation}
        />
        <Spinner show={loading} />
        <FlatList
          style={{ marginTop: 10 }}
          data={Object.keys(sets)}
          renderItem={({ item: id, index: i }) => (
            <TouchableOpacity
              id={sets[id].obj}
              style={{ backgroundColor: '#ffffff', width }}
              onPress={() => this.onPressHandler(sets[id].obj)}
            >
              <View style={[styles.row, { width }]}>
                <Text style={styles.rowText}>{sets[id].name}</Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </Container>
    );
  }
}

SelectSet.propTypes = {
  getSets: PropTypes.func.isRequired,
  getCurrentCameraSet: PropTypes.func.isRequired,
  navigation: PropTypes.object,
  loading: PropTypes.bool.isRequired,
  sets: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  row: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    borderColor: 'black',
  },
  rowText: {
    fontSize: 21,
    ...Platform.select({
      ios: {
        fontFamily: 'STC',
        fontWeight: '400',
      },
      android: {
        fontFamily: 'STC-Bold',
      },
    }),
  },
});

const mapStateToProps = state => ({
  loading: state.cameras.loading,
  sets: state.cameras.sets,
  serverName: state.user.serverName,
});

export default connect(
  mapStateToProps,
  { getSets, getCurrentCameraSet, setCurrentSet }
)(SelectSet);
