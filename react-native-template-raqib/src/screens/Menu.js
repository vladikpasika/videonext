import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  Alert,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Header from '../components/Header';
import { logout } from '../actions/userAction';
import { getDimensions } from '../utils/helpers';

const { width, height } = getDimensions();
const logo = require('../assets/images/logo.png');

const __ = string => {
  return string;
};

const menuItems = [
  {
    name: 'Dashboard',
    route: 'Dashboard',
    border: false,
  },
  {
    name: 'Select Set',
    route: 'SelectSet',
    border: false,
  },
  {
    name: 'Change System',
    route: 'SelectSystem',
    icon: 'video-off',
    border: false,
  },
  {
    name: 'Logout',
    route: 'Logout',
    border: false,
  },
];

class Menu extends Component {
  handlerPress = route => {
    const { logout } = this.props;
    if (route === 'Logout') {
      Alert.alert(
        __('Attention'),
        __('Are you sure, you want to Logout?'),
        [
          { text: __('CANCEL'), onPress: () => null },
          { text: __('OK'), onPress: () => logout() },
        ],
        { cancelable: false }
      );
    }
    return this.props.navigation.navigate(route);
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          width: '100%',
        }}
      >
        <Header
          leftTitle={__(this.props.serverName)}
          leftIcon={true}
          centerTitle={__('Menu')}
          showIconMenu={false}
          navigation={this.props.navigation}
          centerTitleSize={21}
        />
        <View style={styles.container}>
          <TouchableOpacity
            activeOpacity={1}
            style={styles.logoContainer}
            onPress={() => {
              this.props.navigation.goBack();
            }}
          >
            <Image
              resizeMode="contain"
              style={{ width: width * 0.4, height: height * 0.2 }}
              source={logo}
            />
          </TouchableOpacity>
          <View style={{ flex: 3 }}>
            <FlatList
              data={menuItems}
              renderItem={({ item }) => (
                <TouchableOpacity
                  activeOpacity={1}
                  style={styles.menuItem}
                  onPress={() => this.handlerPress(item.route)}
                >
                  <Text style={styles.menuText}>{item.name}</Text>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
  },
  icon: {
    fontSize: 15,
    color: 'black',
  },
  menuItem: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuText: {
    ...Platform.select({
      ios: {
        fontFamily: 'STC',
        fontWeight: '600',
      },
      android: {
        fontFamily: 'STC-Bold',
      },
    }),
    color: 'black',
    fontSize: 25,

    paddingVertical: 10,
  },
  closeContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'yellow',
    paddingVertical: 2,
  },
});

const mapStateToProps = state => ({
  isLandscape: state.user.isLandscape,
  userName: state.user.userName,
  serverAddress: state.user.serverAddress,
  user: state.user,
  serverName: state.user.serverName,
});

Menu.propTypes = {
  isLandscape: PropTypes.bool.isRequired,
  userName: PropTypes.string.isRequired,
  navigation: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

export default connect(
  mapStateToProps,
  { logout }
)(Menu);
