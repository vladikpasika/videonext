import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ color }) => (color ? color : '#fff')};
`;

export const KeyboardAvoidingContainer = styled.KeyboardAvoidingView`
  flex: 1;
  background-color: ${({ color }) => (color ? color : '#fff')};
`;

export const ScrollContainer = styled.ScrollView`
  flex: 1;
  background-color: ${({ color }) => (color ? color : '#fff')};
`;

export const Content = styled.View`
  flex: 1;
  align-items: center;
  padding-horizontal: ${({ paddingLR }) => (paddingLR ? paddingLR : 10)};
  align-items: center;
`;

export const ItemListTouchable = styled.TouchableOpacity`
  height: 68;
  width: ${({ width }) => width * 0.95 || 100};
  padding-horizontal: 10;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-bottom-color: ${({ activeColor }) => activeColor || `#656363`};
  border-bottom-width: 1;
  background-color: ${({ activeColor }) => activeColor || `transparent`};
`;
