import React, { Component } from 'react';
import { StyleSheet, Dimensions, View, Text } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// import MapView from 'react-native-map-clustering';
// import { Marker, ProviderPropType } from 'react-native-maps';
import { Container } from '../styled';
import Header from '../../components/Header';
import Spinner from '../../components/Spinner';
import { getCameraAttributes } from '../../actions/cameras';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class Map extends Component {
  constructor(props) {
    super(props);

    this.state = {
      region: {
        latitude: null,
        longitude: null,
        latitudeDelta: null,
        longitudeDelta: null,
      },
      markers: [],
    };
  }

  componentDidMount() {
    this.props.getCameraAttributes();
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.loading !== this.props.loading &&
      this.props.loading === false
    ) {
      this.getMarkets();
    }
  }

  getMarkets = () => {
    const ids = Object.keys(this.props.listAttr);
    let setRegion = false;
    let markers = [];
    if (ids.length > 0) {
      for (let i = 0; i < ids.length; i++) {
        const cam = this.props.listAttr[ids[i]];
        if (cam.attributes.CAM_GEO_CALIBRATION === 'yes') {
          const lat = Number(cam.attributes.CAM_GEO_LAT);
          const long = Number(cam.attributes.CAM_GEO_LONG);
          if (setRegion === false) {
            this.setState({
              region: {
                latitude: lat,
                longitude: long,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
              },
            });
            setRegion = true;
          }
          markers.push({
            id: cam.obj,
            coordinate: {
              latitude: lat,
              longitude: long,
            },
            title: cam.name,
            description: cam.description,
          });
        }
      }
      if (markers.length > 0) {
        this.setState({ markers });
      }
    }
  };

  render() {
    const { navigation, loading } = this.props;
    const { region, markers } = this.state;
    return (
      <Container color="#9c9c9b">
        <Header navigation={navigation} title="Map" showIcons showLeftIcon />
        <Spinner show={loading} />
        {markers.length > 0 ? (
          <MapView
            region={region}
            style={styles.map}
          >
            {markers.map(item => (
              <Marker
                key={item.id}
                coordinate={item.coordinate}
                title={item.title}
                description={item.description}
              />
            ))}
          </MapView>
        ) : (
          <View>
            <Text style={styles.noData}>No data.</Text>
          </View>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  customView: {
    width: 140,
    height: 100,
  },
  plainView: {
    width: 60,
  },
  map: {
    flex: 1,
  },
  bubble: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
  noData: {
    color: 'white',
    padding: 5,
    fontSize: 16,
  },
});

const mapStateToProps = state => ({
  loading: state.cameras.loading,
  listAttr: state.cameras.listAttr,
});

Map.propTypes = {
  navigation: PropTypes.object.isRequired,
  getCameraAttributes: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  listAttr: PropTypes.object.isRequired,
  provider: ProviderPropType,
};

export default connect(
  mapStateToProps,
  { getCameraAttributes }
)(Map);
