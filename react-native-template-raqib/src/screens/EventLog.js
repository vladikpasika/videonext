import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Container } from './styled';
import Header from '../components/Header';
import Spinner from '../components/spinner';
import EventLogsTable from '../components/EventLogsTable';
import {
  getEventLog,
  setCurrentCamera,
  setCurrentCameraTime,
  clearEventLog,
} from '../actions/cameras';

const __ = string => {
  return string;
};

class EventLog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eventLogPage: 0,
      width: props.deviceWidth,
      height: props.deviceHeight,
      isLandscape: props.isLandscape,
    };
  }

  componentDidMount() {
    this.props.getEventLog(
      {
        size: 20,
        page: 0,
        sort: 'WHEN',
        sortasc: false,
      },
      'loadLogs'
    );
    this.eventLogTimer = this.tickTimer(() => {
      if (Object.keys(this.props.eventsLogs).length) {
        this.props.getEventLog(
          {
            size: Object.keys(this.props.eventsLogs).length,
            page: 0,
            sort: 'WHEN',
            sortasc: false,
          },
          'updateLogs'
        );
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { eventLogPage } = this.state;
    if (prevState.eventLogPage !== eventLogPage && eventLogPage !== 0) {
      this.props.getEventLog(
        {
          size: 20,
          page: eventLogPage,
          sort: 'WHEN',
          sortasc: false,
        },
        'updateLogs'
      );
    }

    if (prevProps.deviceWidth !== this.props.deviceWidth) {
      this.setState({
        height: this.props.deviceHeight,
      });
    }

    if (prevProps.deviceHeight !== this.props.deviceHeight) {
      this.setState({
        height: this.props.deviceHeight,
      });
    }

    if (prevProps.isLandscape !== this.props.isLandscape) {
      this.setState({
        height: this.props.isLandscape,
      });
    }
  }

  componentWillUnmount() {
    clearInterval(this.eventLogTimer);
  }

  tickTimer = func => setInterval(func, 20000);

  logPressHandler = id => {
    const { eventsLogs } = this.props;
    this.props.setCurrentCamera(eventsLogs[id].object.objId);
    this.props.setCurrentCameraTime({
      startTime: eventsLogs[id].from / 1000,
      endTime: eventsLogs[id].to / 1000,
    });
    this.props.navigation.navigate('CameraLiveView', { isArchive: true });
  };

  onFlatListEndReached = () => {
    if (!this.props.loading) {
      this.setState({ eventLogPage: this.state.eventLogPage + 1 });
    }
  };

  render() {
    let { navigation, eventsLogs, listCameras, currentCameraTime } = this.props;
    const { isLandscape } = this.state;

    return (
      <Container color="#ffffff">
        <Header
          navigation={navigation}
          centerTitle={__('Events on cctv-stg.stcs.com.sa')}
          col={2}
          showIconMenu
        />
        <Spinner show={this.props.loading} />
        <EventLogsTable
          isLandscape={isLandscape}
          eventsLogs={eventsLogs}
          navigation={navigation}
          logPressHandler={this.logPressHandler}
          listCameras={listCameras}
          currentCameraTime={currentCameraTime}
          onFlatListEndReached={this.onFlatListEndReached}
        />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.cameras.loading,
  eventsLogs: state.cameras.eventLogs,
  deviceWidth: state.user.deviceWidth,
  deviceHeight: state.user.deviceHeight,
  isLandscape: state.user.isLandscape,
  listCameras: state.cameras.list,
  currentCameraTime: state.cameras.currentCameraTime,
});

EventLog.propTypes = {
  eventsLogs: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  getEventLog: PropTypes.func.isRequired,
  deviceWidth: PropTypes.number.isRequired,
  deviceHeight: PropTypes.number.isRequired,
  isLandscape: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  setCurrentCamera: PropTypes.func.isRequired,
  setCurrentCameraTime: PropTypes.func.isRequired,
  listCameras: PropTypes.object.isRequired,
  currentCameraTime: PropTypes.object,
  clearEventLog: PropTypes.func,
  user: PropTypes.object,
};

export default connect(
  mapStateToProps,
  {
    getEventLog,
    setCurrentCameraTime,
    setCurrentCamera,
    clearEventLog,
  }
)(EventLog);
