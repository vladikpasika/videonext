import React, { Component } from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { getDimensions } from '../utils/helpers';

const { height } = getDimensions();

export default class Spinner extends Component {
  render() {
    const { show } = this.props;
    if (show) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#ffffff" />
        </View>
      );
    }
    return null;
  }
}

const styles = StyleSheet.create({
  container: {
    height,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    zIndex: 10000,
  },
});

Spinner.propTypes = {
  show: PropTypes.bool.isRequired,
};
