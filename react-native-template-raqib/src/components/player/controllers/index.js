import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import {
  ActivityIndicator,
  TouchableHighlight,
  View,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
//Components
import Chevrons from './chevrons';
// import FullScreenIcon from './fullScreenIcon';

const Controllers = ({
  width,
  height,
  isPlaying,
  play,
  stop,
  iconPlaySize = 10,
  fScreen,
  playerMessage,
  //below props only for Chevrons
  logPressHandler,
  eLogsLength,
  currentLog,
  isVisible,
  eLogsKeys,
  fullScreen,
  defaultScreen,
  archiveIndicator,
  archiveSwitch,
  liveRecord,
}) => (
  <View
    style={[
      styles.playerControllersWrap,
      {
        width,
        height,
        justifyContent: !liveRecord ? 'center' : 'space-between',
      },
    ]}
  >
    {isVisible && eLogsLength ? (
      <Chevrons
        iconName="chevrons-left"
        logPressHandler={logPressHandler}
        eLogsLength={eLogsLength}
        currentLog={currentLog}
        eLogsKeys={eLogsKeys}
      />
    ) : null}
    <TouchableHighlight
      onPress={(!isPlaying && play) || stop}
      // onPress={() => {console.log('press pause')}}
      underlayColor="transparent"
      activeOpacity={0.5}
    >
      <View style={{ backgroundColor: 'transparent' }}>
        {(isVisible && !isPlaying ? (
          <View>
            <Icon name="play" size={iconPlaySize} color="#DAE2E7" />
          </View>
        ) : null) /*||
          (isPlaying && playerMessage === 'OPENNING' ? (
            <ActivityIndicator size="large" color="#ffffff" />
          ) : null)*/ ||
          (isVisible && !liveRecord ? (
            <View>
              <Icon
                style={[styles.playPausePos1]}
                name="pause"
                size={iconPlaySize}
                color="#DAE2E7"
              />
            </View>
          ) : null)}
      </View>
    </TouchableHighlight>
    {isVisible && eLogsLength ? (
      <Chevrons
        iconName="chevrons-right"
        logPressHandler={logPressHandler}
        eLogsLength={eLogsLength}
        currentLog={currentLog}
        eLogsKeys={eLogsKeys}
      />
    ) : null}
    {archiveIndicator}
    {isVisible ? archiveSwitch : null}
    {/* <FullScreenIcon
        defaultScreen={defaultScreen}
        fullScreen={fullScreen}
        fScreen={fScreen}
        isVisible={isVisible}
      />*/}
  </View>
);

const styles = StyleSheet.create({
  playerControllersWrap: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    position: 'absolute',
    paddingLeft: 50,
    paddingRight: 50,
    backgroundColor: 'transparent',
    zIndex: 1000,
  },
  playPausePos1: {},
  playPausePos2: {
    transform: [{ rotate: '90deg' }],
  },
});

Controllers.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  isPlaying: PropTypes.bool.isRequired,
  fScreen: PropTypes.bool.isRequired,
  playerMessage: PropTypes.string.isRequired,
  iconPlaySize: PropTypes.number,
  play: PropTypes.func.isRequired,
  stop: PropTypes.func.isRequired,
  logPressHandler: PropTypes.func.isRequired,
  eLogsLength: PropTypes.number.isRequired,
  currentLog: PropTypes.number.isRequired,
  isVisible: PropTypes.bool.isRequired,
  eLogsKeys: PropTypes.array.isRequired,
  defaultScreen: PropTypes.func.isRequired,
  fullScreen: PropTypes.func.isRequired,
  archiveIndicator: PropTypes.element.isRequired,
  archiveSwitch: PropTypes.element.isRequired,
  liveRecord: PropTypes.bool.isRequired,
};

export default Controllers;
