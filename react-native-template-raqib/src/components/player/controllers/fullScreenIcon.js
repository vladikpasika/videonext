import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import { TouchableHighlight, View, StyleSheet } from 'react-native';

import PropTypes from 'prop-types';

const FullScreenIcon = ({ fScreen, fullScreen, defaultScreen, isVisible }) => (
  <TouchableHighlight
    underlayColor="transparent"
    activeOpacity={0.5}
    style={[
      styles.fullScreenButton,
      styles.fullScreenIconPos1,
      { height: 'auto' },
    ]}
    onPress={fScreen ? defaultScreen : fullScreen}
  >
    <View>
      <Icon name={fScreen ? 'minimize' : 'maximize'} size={30} color="#fff" />
    </View>
  </TouchableHighlight>
);

const styles = StyleSheet.create({
  fullScreenButton: {
    zIndex: 1003,
    position: 'absolute',
    right: 10,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  fullScreenIconPos1: {
    top: 10,
  },
  fullScreenIconPos2: {
    bottom: 10,
  },
});

FullScreenIcon.propTypes = {
  fScreen: PropTypes.bool.isRequired,
  fullScreen: PropTypes.func.isRequired,
  defaultScreen: PropTypes.func.isRequired,
  isVisible: PropTypes.bool.isRequired,
};

export default FullScreenIcon;
