import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import { TouchableHighlight, View } from 'react-native';

import PropTypes from 'prop-types';

const Chevrons = ({
  eLogsLength,
  logPressHandler,
  currentLog,
  iconName,
  eLogsKeys,
}) => {
  const isLeft = iconName === 'chevrons-left';
  let nextLog,
    currentLogEventId = 0;
  if (currentLog) {
    currentLogEventId = eLogsKeys.indexOf(`${currentLog}`);
  }
  if (isLeft) {
    nextLog =
      isLeft && currentLogEventId === 0
        ? eLogsLength - 1
        : currentLogEventId - 1;
  } else {
    nextLog =
      !isLeft && currentLogEventId === eLogsLength - 1
        ? 0
        : currentLogEventId + 1;
  }

  return (
    <TouchableHighlight
      underlayColor="transparent"
      activeOpacity={0.5}
      onPress={() => {
        logPressHandler(eLogsKeys[nextLog]);
      }}
    >
      <View>
        <Icon name={iconName} size={40} color="rgba(218,226,231,0.75)" />
      </View>
    </TouchableHighlight>
  );
};

Chevrons.propTypes = {
  eLogsKeys: PropTypes.array.isRequired,
  eLogsLength: PropTypes.number.isRequired,
  logPressHandler: PropTypes.func.isRequired,
  currentLog: PropTypes.number.isRequired,
  iconName: PropTypes.string.isRequired,
};

export default Chevrons;
