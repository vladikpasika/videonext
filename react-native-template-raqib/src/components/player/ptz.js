import React, { Component } from 'react';
import { View, StyleSheet, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Feather';
import PropTypes from 'prop-types';
import Joystick from '../../utils/Joystick';
import Ball from './Ball';
import Slider from './slider';
import { getDimensions } from '../../utils/helpers';

const { width, height } = getDimensions();

class PTZ extends Component {
  constructor(props) {
    super(props);
    Joystick.init();
    Joystick.setObj(this.props.currentCamera);
    this.state = {
      typePtz: 'grid',
    };
  }

  getZoomValue = value => {
    const srvCommand = Joystick.onAxis('3', value * 0.2);
    srvCommand.then(command => {
      this.props.handlePtzPress(command);
    });
  };

  getCoordinates = (x, y) => {
    Joystick.onAxis('0', x * 0.2);
    const srvCommand = Joystick.onAxis('1', -(y * 0.2));
    srvCommand.then(command => {
      this.props.handlePtzPress(command);
    });
  };

  handlePtzPress = (position, command) => {
    // if (position === 'x') {
    //   // this.setShowPtz(false);
    // } else
    if (position === 'move') {
      this.setState({ typePtz: 'move' });
    } else if (position === 'grid') {
      this.setState({ typePtz: 'grid' });
    } else if (position !== 'circle') {
      const srvCommand = Joystick.sendCommand(command[0]);
      srvCommand.then(command => {
        this.props.handlePtzPress(command);
      });
    }
  };

  renderControllItem(position, isSmall, command = null) {
    const { isLandscape } = this.props;
    let style;
    if (isLandscape) {
      style = styles.iconSmallerContainer;
    } else {
      style = isSmall ? styles.iconSmallerContainer : styles.iconContainer;
    }
    return (
      <TouchableHighlight
        style={style}
        onPress={() => this.handlePtzPress(position, command)}
      >
        <Icon
          name={position}
          style={isSmall ? styles.iconSmaller : styles.icon}
        />
      </TouchableHighlight>
    );
  }

  renderGrid() {
    const { isLandscape } = this.props;
    return (
      <Grid>
        <Col>
          <Row style={styles.row}>
            {/*{this.renderControllItem('arrow-up-left')}*/}
          </Row>
          <Row style={styles.row}>
            {this.renderControllItem('arrow-left', false, [
              'mode=step&move=left',
            ])}
          </Row>
          <Row style={styles.row}>
            {/*{this.renderControllItem('arrow-down-left')}*/}
          </Row>
        </Col>
        <Col>
          <Row style={styles.row}>
            {this.renderControllItem('arrow-up', false, ['mode=step&move=up'])}
          </Row>
          <Row style={styles.row}>{this.renderControllItem('circle')}</Row>
          <Row style={styles.row}>
            {this.renderControllItem('arrow-down', false, [
              'mode=step&move=down',
            ])}
          </Row>
        </Col>
        <Col>
          <Row style={styles.row}>
            {/*{this.renderControllItem('arrow-up-right')}*/}
          </Row>
          <Row style={styles.row}>
            {this.renderControllItem('arrow-right', false, [
              'mode=step&move=right',
            ])}
          </Row>
          <Row style={styles.row}>
            {/*{this.renderControllItem('arrow-down-right')}*/}
          </Row>
        </Col>
        <Col>
          {/*<Row style={styles.row}>*/}
          {/*{this.renderControllItem('x', true)}*/}
          {/*</Row>*/}
          <Row style={styles.row}>
            {this.renderControllItem('plus', true, ['mode=step&zoom=in'])}
          </Row>
          <Row style={styles.row}>
            {this.renderControllItem('minus', true, ['mode=step&zoom=out'])}
          </Row>
          {!isLandscape && (
            <Row style={styles.row}>
              {this.renderControllItem('move', true)}
            </Row>
          )}
        </Col>
      </Grid>
    );
  }

  renderMove(width, height) {
    // const { locationX, locationY } = this.state;
    return (
      <Grid>
        <Col size={3}>
          <Row
            style={[styles.row, { backgroundColor: '#501f6e', padding: 10 }]}
          >
            <Ball
              getCoordinates={this.getCoordinates}
              parentWidth={width * 0.75}
              parentHeight={height}
            />
          </Row>
        </Col>
        <Col size={1}>
          <Row
            size={5}
            style={[styles.row, { backgroundColor: 'transparent' }]}
          >
            <Slider getZoomValue={this.getZoomValue} />
          </Row>
          <Row size={1} style={styles.row}>
            {this.renderControllItem('grid', true)}
          </Row>
        </Col>
      </Grid>
    );
  }

  renderModal = () => {
    const { typePtz } = this.state;
    const { isLandscape } = this.props;

    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          width: width,
          backgroundColor: 'transparent',
        }}
      >
        {this.props.ptz !== 'none' && (
          <View
            style={{
              paddingHorizontal: !isLandscape ? width * 0.1 : 0,
              backgroundColor: 'transparent',
              width,
              marginTop: 0,
              marginLeft: 0,
              marginRight: 0,
              height: height * 0.35,
            }}
          >
            {typePtz === 'grid'
              ? this.renderGrid()
              : this.renderMove(width * 0.8, height * 0.35)}
          </View>
        )}
      </View>
    );
  };

  render() {
    const { deviceWidth, deviceHeight, isLandscape } = this.props;

    return (
      <View
        style={{
          zIndex: 1000,
          position: 'absolute',
          backgroundColor: 'transparent',
          width: deviceWidth,
          height: deviceHeight,
          marginTop: !isLandscape ? 50 : 0,
        }}
      >
        {this.renderModal()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    backgroundColor: '#501f6e',
    padding: 20,
  },
  icon: {
    fontSize: 25,
    color: 'white',
  },
  iconSmallerContainer: {
    backgroundColor: '#501f6e',
    padding: 10,
  },
  iconSmaller: {
    fontSize: 15,
    color: 'white',
  },
});

PTZ.propTypes = {
  deviceWidth: PropTypes.number.isRequired,
  deviceHeight: PropTypes.number.isRequired,
  showPtz: PropTypes.bool.isRequired,
  isLandscape: PropTypes.bool.isRequired,
  currentCamera: PropTypes.string.isRequired,
  ptz: PropTypes.string.isRequired,
  handlePtzPress: PropTypes.func.isRequired,
};

const mapStateToProps = ({
  user: { deviceWidth, deviceHeight, isLandscape },
}) => ({
  deviceWidth,
  deviceHeight,
  isLandscape,
});

export default connect(mapStateToProps)(PTZ);
