import React, { Component } from 'react';
import { PanResponder, StyleSheet, View, Animated, Text } from 'react-native';
import type { PanResponderInstance, GestureState } from 'PanResponder';
import type { PressEvent } from 'CoreEventTypes';
import { getDimensions } from '../../utils/helpers';

const { width, height } = getDimensions();

type CircleStyles = {
  backgroundColor?: string,
  left?: number,
  top?: number,
};

const CIRCLE_SIZE = 40;

export default class Ball extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  _handleStartShouldSetPanResponder = (
    event: PressEvent,
    gestureState: GestureState
  ): boolean => {
    return true;
  };

  _handleMoveShouldSetPanResponder = (
    event: PressEvent,
    gestureState: GestureState
  ): boolean => {
    return true;
  };

  _handlePanResponderGrant = (
    event: PressEvent,
    gestureState: GestureState
  ) => {
    this._highlight();
  };

  _handlePanResponderMove = (event: PressEvent, gestureState: GestureState) => {
    if (this.circle) {
      this.circle.measure((fx, fy, w, h, px, py) => {
        const xMin = 10; const xMax = 58; const yMin = 50; const yMax = 80;
        let x = (px * 100) / width;
        let y = (py * 100) / height;
        if (x > xMin && x < xMax && (y > yMin && y < yMax)) {
          const xp = this._getCoordinate(xMin, xMax, x);
          const yp = this._getCoordinate(yMin, yMax, y);
          this.props.getCoordinates(xp, yp);
          this._circleStyles.style.left = this._previousLeft + gestureState.dx;
          this._circleStyles.style.top = this._previousTop + gestureState.dy;
          this._updateNativeStyles();
        } else {
          this._startingPosition();
        }
      });
    }
  };

  _handlePanResponderEnd = (event: PressEvent, gestureState: GestureState) => {
    this._unHighlight();
    this._previousLeft += gestureState.dx;
    this._previousTop += gestureState.dy;
    this.props.getCoordinates(0, 0);
    setTimeout(() => this.props.getCoordinates(0, 0), 500);
    this._startingPosition();
  };

  _panResponder: PanResponderInstance = PanResponder.create({
    onStartShouldSetPanResponder: this._handleStartShouldSetPanResponder,
    onMoveShouldSetPanResponder: this._handleMoveShouldSetPanResponder,
    onPanResponderGrant: this._handlePanResponderGrant,
    onPanResponderMove: this._handlePanResponderMove,
    onPanResponderRelease: this._handlePanResponderEnd,
    onPanResponderTerminate: this._handlePanResponderEnd,
  });

  _previousLeft: number = 0;
  _previousTop: number = 0;
  _circleStyles: {| style: CircleStyles |} = { style: {} };
  circle: ?React.ElementRef<typeof View> = null;

  UNSAFE_componentWillMount() {
    this._startingPosition();
  }

  componentDidMount() {
    this._updateNativeStyles();
  }

  _startingPosition() {
    this._previousLeft = width / 4.4;
    this._previousTop = -(height / 33.35);
    this._circleStyles = {
      style: {
        left: this._previousLeft,
        top: this._previousTop,
        backgroundColor: 'white',
      },
    };
    this._updateNativeStyles();
  }

  _highlight() {
    this._circleStyles.style.backgroundColor = 'white';
    this._updateNativeStyles();
  }

  _unHighlight() {
    this._circleStyles.style.backgroundColor = 'white';
    this._updateNativeStyles();
  }

  _updateNativeStyles() {
    this.circle && this.circle.setNativeProps(this._circleStyles);
  }

  _getCoordinate = (min, max, v) => {
    const length = max - min;
    v = v - min;
    let vp = (v / (length / 2)) * 100;
    vp = vp > 200 ? 200 : vp;
    return Math.ceil(vp - 100);
  };

  render() {
    return (
      <Animated.View
        style={[styles.container]}
      >
        <View
          ref={circle => {
            this.circle = circle;
          }}
          style={styles.circle}
          {...this._panResponder.panHandlers}
        />
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  circle: {
    width: CIRCLE_SIZE,
    height: CIRCLE_SIZE,
    borderRadius: CIRCLE_SIZE / 2,
    position: 'absolute',
    zIndex: 1010,
    left: 0,
    top: 0,
  },
  container: {
    flex: 1,
  },
});
