import React, { PureComponent } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  AppState,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import Orientation from 'react-native-orientation';
import VnPlayer from 'react-native-vn-player';
import ArchiveDatePicker from '../ArchiveDatePicker';
import { getDimensions } from '../../utils/helpers';
//Components
import Controllers from './controllers';
import DateTimeBlock, { Text } from './dateTimeBlock';
import PtzIcon from './ptzIcon';
import Message from './helpers';

const moment = require('moment');

const { width: playerDefaultWidth, height, scale } = getDimensions();
const playerDefaultHeight = height * 0.35;
const iconPlaySize = 50;

class Player extends PureComponent {
  constructor(props) {
    super(props);
    this.player = React.createRef();
    this.prevPlayingCondition = false;
    this.state = {
      isPlaying: false,
      isVisible: false,
      playerMessage: '',
      videoIsOpened: false,
      datetime: '',
      playerPreview: '',
      width: playerDefaultWidth,
      height: playerDefaultHeight,
      contPlayerWidth: playerDefaultWidth,
      contPlayerHeight: playerDefaultHeight,
      fScreen: false,
      showCalendar: false,
    };
    this.timeOutHandler = null;
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    const { currentCameraTime } = this.props;
    if (currentCameraTime.startTime !== undefined) {
      this.setState({
        datetime: this.setDateTimeFormat(currentCameraTime.startTime),
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.currentCameraTime.startTime !==
      this.props.currentCameraTime.startTime
    ) {
      const { currentCameraTime } = this.props;
      this.setState({
        datetime: this.setDateTimeFormat(currentCameraTime.startTime),
      });
    }
    if (prevProps.mediaUrlLoading !== this.props.mediaUrlLoading) {
      if (this.props.mediaUrlLoading) {
        this.stop();
      } else {
        this.play();
      }
    }
    if (prevProps.isLandscape !== this.props.isLandscape) {
      let biggerVal =
        this.props.deviceWidth > this.props.deviceHeight
          ? this.props.deviceWidth
          : this.props.deviceHeight;
      let lowerVal =
        this.props.deviceWidth < this.props.deviceHeight
          ? this.props.deviceWidth
          : this.props.deviceHeight;
      if (this.props.isLandscape === true) {
        this.setState({
          fScreen: true,
          width: biggerVal,
          height: lowerVal,
          contPlayerWidth: biggerVal,
          contPlayerHeight: lowerVal,
        });
      } else {
        this.setState({
          fScreen: false,
          width: playerDefaultWidth,
          height: playerDefaultHeight,
          contPlayerWidth: playerDefaultWidth,
          contPlayerHeight: playerDefaultHeight,
        });
      }
      this.player.current.fullscreen(this.props.isLandscape);
      // this.props.reloadPlayer();
      // this.props.getMediaUrl();
      Orientation.unlockAllOrientations();
    }
  }

  componentWillUnmount() {
    Orientation.unlockAllOrientations();
    AppState.removeEventListener('change', this._handleAppStateChange);
    clearInterval(this.timeOutHandler);
    this.player.current.fullscreen(false);
    this.player.current.play(false);
  }

  setDateTimeFormat = datetime => {
    if (Platform.OS === 'ios') {
      return moment(datetime).format('dddd, MMMM DD YYYY, H:mm:ss');
    } else {
      const t = new Date(datetime * 1);
      return moment(t).format('dddd, MMMM DD YYYY, H:mm:ss');
    }
  };

  timeOutFunc = (func, time = 3000) => setTimeout(func, time);

  onStateChanged = event => {
    let message = event.state;
    // if (event.state === 'OPENNING' && this.state.videoIsOpened) {
    //   this.setState({ videoIsOpened: false });
    // } else if (event.state === 'PLAY_FROM_SERVER') {
    //   this.setState({ videoIsOpened: true });
    // }
    if (event.error !== undefined) {
      message = event.error;
      // todo uncomment in production
      // this.stop();
      // this.timeOutHandler = this.timeOutFunc(() => this.play());
    }

    // message for debugging
    this.setState({
      playerMessage: message !== 'PLAY_FROM_SERVER' ? message : '',
    });
    if (message === 'Authorization failed') {
      this.props.getMediaUrl();
    }
  };

  // onNewStream = event => {
  //   console.log('onNewStream:', event);
  // };

  onNewFrame = event => {
    this.setState({ datetime: this.setDateTimeFormat(event.pts) });
  };

  play = () => {
    if (this.player && this.player.current) {
      this.player.current.play(true);
      this.setState({ isPlaying: true });
    }
  };

  makeControllersVisible = () => {
    if (!this.state.isVisible) {
      this.setState({ isVisible: true });
      this.props.isVisible(true);
      setTimeout(() => {
        this.setState({ isVisible: false });
        this.props.isVisible(false);
      }, 3000);
    } else {
      this.setState({ isVisible: false });
      this.props.isVisible(false);
    }
  };

  stop = () => {
    this.player.current.play(false);
    this.setState({ isPlaying: false, playerMessage: '', datetime: '' });
  };

  fullScreen = () => {
    Orientation.lockToLandscape();
    // Orientation.unlockAllOrientations();
    this.setState({ fScreen: true });
  };

  defaultScreen = () => {
    Orientation.lockToPortrait();
    // Orientation.unlockAllOrientations();
    this.setState({ fScreen: false });
  };

  setCalendar = value => {
    this.props.setCalendar(value);
  };

  _handleAppStateChange = nextAppState => {
    if (nextAppState === 'active' && this.prevPlayingCondition) {
      if (this.prevPlayingCondition) {
        this.prevPlayingCondition = this.setState.isPlaying;
        this.play();
      }
    } else if (nextAppState === 'inactive' && this.state.isPlaying) {
      this.prevPlayingCondition = this.state.isPlaying;
      this.stop();
    } else if (nextAppState === 'background' && this.state.isPlaying) {
      this.prevPlayingCondition = this.state.isPlaying;
      this.stop();
    }
  };

  // _handlePresCalendarIcon = () => {
  //   if (this.state.isPlaying) {
  //     this.stop();
  //   }
  //   this.setState({ showCalendar: true });
  // };

  _handleDatePicked = date => {
    const startTime = moment(date).format('X');
    const endTime = moment(date)
      .add(1, 'hours')
      .format('X');
    this.props.getArchiveMediaUrl(startTime, endTime);
    this.setCalendar(false);
  };

  render() {
    const {
      width,
      height,
      fScreen,
      contPlayerHeight,
      contPlayerWidth,
    } = this.state;
    return (
      <TouchableOpacity
        style={{ backgroundColor: 'transparent' }}
        onPress={this.makeControllersVisible}
        underlayColor="transparent"
        activeOpacity={0.9}
      >
        {/*{!this.state.videoIsOpened && (*/}
        {/*<Image*/}
        {/*style={[styles.previewImage, {width, height}]}*/}
        {/*resizeMode="cover"*/}
        {/*source={{*/}
        {/*uri: this.props.image,*/}
        {/*}}*/}
        {/*/>*/}
        {/*)}*/}
        <View style={[styles.vlcplayer, { width, height: contPlayerHeight }]}>
          <VnPlayer
            key={this.props.playerKey}
            ref={this.player}
            play={false}
            source={{
              uri: this.props.playerUri,
              width:
                Platform.OS === 'ios'
                  ? contPlayerWidth
                  : contPlayerWidth * scale,
              height: Platform.OS === 'ios' ? height : height * scale,
              // width: contPlayerWidth ,
              // height: height,
            }}
            onStateChanged={this.onStateChanged}
            onNewStream={this.onNewStream}
            onNewFrame={this.onNewFrame}
          />
        </View>
        <ArchiveDatePicker
          setCalendar={this.setCalendar}
          show={this.props.showCalendar}
          _handleDatePicked={this._handleDatePicked}
        />
        <Controllers
          archiveIndicator={this.props.archiveIndicator}
          archiveSwitch={this.props.archiveSwitch}
          width={width}
          height={height}
          isPlaying={this.state.isPlaying}
          play={this.play}
          stop={this.stop}
          fScreen={fScreen}
          currentLog={this.props.currentLog}
          playerMessage={this.state.playerMessage}
          logPressHandler={this.props.logPressHandler}
          eLogsLength={this.props.eLogsLength}
          eLogsKeys={this.props.eLogsKeys}
          liveRecord={this.props.liveRecord}
          iconPlaySize={iconPlaySize}
          isVisible={this.state.isVisible}
          defaultScreen={this.defaultScreen}
          fullScreen={this.fullScreen}
        />
        <DateTimeBlock
          datetime={this.state.datetime}
          isVisible={this.state.isVisible}
          fScreen={fScreen}
          // _handlePresCalendarIcon={this._handlePresCalendarIcon}
        />
        {/*<PtzIcon*/}
          {/*isVisible={this.state.isVisible}*/}
          {/*liveRecord={this.props.liveRecord}*/}
          {/*ptz={this.props.ptz}*/}
          {/*isLandscape={this.props.isLandscape}*/}
          {/*ptzIconPressHandler={this.props.ptzIconPressHandler}*/}
        {/*/>*/}
        <Message playerMessage={this.state.playerMessage} width={width} />
      </TouchableOpacity>
    );
  }
}

Player.propTypes = {
  playerUri: PropTypes.string.isRequired,
  eLogsLength: PropTypes.number,
  currentCameraTime: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  currentLog: PropTypes.number,
  logPressHandler: PropTypes.func,
  loading: PropTypes.bool.isRequired,
  image: PropTypes.string.isRequired,
  mediaUrlLoading: PropTypes.bool.isRequired,
  deviceWidth: PropTypes.number.isRequired,
  deviceHeight: PropTypes.number.isRequired,
  setFullScreen: PropTypes.func,
  getArchiveMediaUrl: PropTypes.func.isRequired,
  isLandscape: PropTypes.bool.isRequired,
  getMediaUrl: PropTypes.func.isRequired,
  eLogsKeys: PropTypes.array.isRequired,
  playerKey: PropTypes.number.isRequired,
  reloadPlayer: PropTypes.func.isRequired,
  archiveIndicator: PropTypes.element.isRequired,
  archiveSwitch: PropTypes.element.isRequired,
  showCalendar: PropTypes.bool,
  setCalendar: PropTypes.func,
  isVisible: PropTypes.func,
  ptz: PropTypes.string.isRequired,
  liveRecord: PropTypes.bool,
  ptzIconPressHandler: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  // previewImage: {
  //   position: 'absolute',
  // },
  welcome: {
    fontSize: 10,
    textAlign: 'center',
    margin: 20,
  },
  vlcplayer: {
    zIndex: 1000,
    backgroundColor: '#c6c6c6',
  },
});

export default Player;
