import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
  Animated,
  TouchableOpacity,
  Modal,
  PanResponder,
} from 'react-native';
import PropTypes from 'prop-types';
import { getDimensions } from '../../utils/helpers';

const { width, height } = getDimensions();

class PtzIcon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPtz: false,
      prevStatusShowPtz: null,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.isLandscape !== this.props.isLandscape &&
      this.props.isLandscape === true
    ) {
      this.setState({ showPtz: false, prevStatusShowPtz: this.state.showPtz });
      this.props.ptzIconPressHandler(false);
    }

    if (
      prevProps.isLandscape !== this.props.isLandscape &&
      this.props.isLandscape === false &&
      this.state.prevStatusShowPtz !== null
    ) {
      this.setState({
        showPtz: this.state.prevStatusShowPtz,
        prevStatusShowPtz: null,
      });
      this.props.ptzIconPressHandler(this.state.prevStatusShowPtz);
    }
  }

  ptzIconPressHandler = () => {
    this.setState({ showPtz: !this.state.showPtz });
    this.props.ptzIconPressHandler(!this.state.showPtz);
  };

  renderPtzIcon = () => {
    const { isLandscape, ptz } = this.props;
    if (ptz !== 'none' && !isLandscape) {
      return (
        <View
          style={[
            styles.ptzContainer,
            {
              right: isLandscape ? height * 0.05 - 10 : width * 0.15,
            },
          ]}
        >
          <Text
            onPress={() => this.ptzIconPressHandler()}
            style={styles.ptzIcon}
          >
            PTZ
          </Text>
        </View>
      );
    }
    return null;
  };

  render() {
    const { isVisible, liveRecord } = this.props;

    return (
      <View
        style={{
          zIndex: 1000,
          position: 'absolute',
          backgroundColor: 'transparent',
          width,
          height,
        }}
      >
        {isVisible && liveRecord && this.renderPtzIcon()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ptzContainer: {
    zIndex: 1000,
    position: 'absolute',
    top: 13,
    backgroundColor: 'rgba(0,0,0,0.5)',
    borderColor: '#fff',
    borderWidth: 2,
    borderRadius: 8,
  },
  ptzIcon: {
    color: '#fff',
    fontSize: 11,
    padding: 3,
  },
  row: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    backgroundColor: '#501f6e',
    padding: 20,
  },
  icon: {
    fontSize: 25,
    color: 'white',
  },
  iconSmallerContainer: {
    backgroundColor: '#501f6e',
    padding: 10,
  },
  iconSmaller: {
    fontSize: 15,
    color: 'white',
  },
});

PtzIcon.propTypes = {
  isLandscape: PropTypes.bool.isRequired,
  isVisible: PropTypes.bool,
  liveRecord: PropTypes.bool,
  ptzIconPressHandler: PropTypes.func.isRequired,
};

export default PtzIcon;
