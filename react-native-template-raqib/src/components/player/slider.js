import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Modal,
  TouchableHighlight,
  Animated,
  TouchableOpacity,
} from 'react-native';
import { Slider } from 'react-native-elements';
import { getDimensions } from '../../utils/helpers';

const { width, height } = getDimensions();
const sliderMarginLeft = -(width / 6.6);
const sliderWidth = width / 1.97;

class SliderZoom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
    };
  }

  onValueChange = value => {
    this.setState({ value });
    this.props.getZoomValue(value * -1);
  };

  onSlidingComplete = value => {
    this.setState({ value: 0 });
    this.props.getZoomValue(0);
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          zIndex: 1002,
          backgroundColor: 'transparent',
          height: sliderMarginLeft,
        }}
      >
        <Slider
          style={{
            width: sliderWidth,
            marginLeft: sliderMarginLeft,
            marginTop: 5,
            marginBottom: 5,
            backgroundColor: 'transparent',
          }}
          trackStyle={{ width: sliderWidth }}
          thumbStyle={{ backgroundColor: '#501f6e', height: 25, width: 25 }}
          maximumValue={100}
          minimumValue={-100}
          step={1}
          orientation={'vertical'}
          value={this.state.value}
          onValueChange={value => this.onValueChange(value)}
          onSlidingComplete={value => this.onSlidingComplete(value)}
        />
      </View>
    );
  }
}

export default SliderZoom;
