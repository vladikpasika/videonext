import React from 'react';
import { Text, View, StyleSheet, Platform } from 'react-native';

import PropTypes from 'prop-types';

const Message = ({ playerMessage, width }) => (
  <View style={[styles.playerMessage, { width: width * 0.7 }]}>
    <Text style={styles.messageText}>{playerMessage}</Text>
  </View>
);

const styles = StyleSheet.create({
  playerMessage: {
    zIndex: 1000,
    position: 'absolute',
    top: 10,
    left: 10,
    backgroundColor: 'transparent',
  },
  messageText: {
    color: '#DAE2E7',
    fontSize: 16,
    ...Platform.select({
      ios: {
        fontFamily: 'STC',
      },
      android: {
        fontFamily: 'STC-Regular',
      },
    }),
  },
});

Message.propTypes = {
  playerMessage: PropTypes.string.isRequired,
};

export default Message;
