import React from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  View,
  Platform,
} from 'react-native';

import PropTypes from 'prop-types';

const DateTimeBlock = ({
  datetime,
  // _handlePresCalendarIcon,
  isVisible,
  fScreen,
}) => (
  <TouchableOpacity
    style={[
      styles.datetime,
      styles.datetimePos1,
      { zIndex: isVisible ? 1000 : 0 },
    ]}
    underlayColor="transparent"
    activeOpacity={0.5}
    // onPress={_handlePresCalendarIcon}
  >
    <View styles={styles.messageWrap}>
      <Text style={[styles.messageText, { fontSize: fScreen ? 23 : 16 }]}>
        {` ${datetime}`}
      </Text>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  datetime: {
    zIndex: 1000,
    position: 'absolute',
    backgroundColor: 'transparent',
    paddingTop: 30,
    paddingRight: 30,
  },
  datetimePos1: {
    top: 0,
    left: 30,
  },
  datetimePos2: {
    top: 110,
    right: -100,
    transform: [{ rotate: '90deg' }],
  },
  messageText: {
    color: '#DAE2E7',
    fontSize: 25,
    ...Platform.select({
      ios: {
        fontFamily: 'STC',
      },
      android: {
        fontFamily: 'STC-Regular',
      },
    }),
  },
  messageWrap: {
    flexDirection: 'row',
  },
});

DateTimeBlock.propTypes = {
  datetime: PropTypes.string.isRequired,
  // _handlePresCalendarIcon: PropTypes.func.isRequired,
  isVisible: PropTypes.bool.isRequired,
  fScreen: PropTypes.bool.isRequired,
};

export default DateTimeBlock;
