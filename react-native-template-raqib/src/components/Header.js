import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Platform,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather';
import { Header } from 'react-navigation';

const { width, height } = Dimensions.get('window');

class AppHeader extends Component {
  titlePress = () => {
    const { titlePressHandler } = this.props;
    if (titlePressHandler !== undefined) {
      titlePressHandler();
    }
  };

  render() {
    const {
      leftTitle = '',
      centerTitle = '',
      col = 3,
      navigation,
      isLandscape,
      visible = true,
      showIconMenu = false,
      marginBottom = 0,
      leftIcon = false,
    } = this.props;
    const headerHeight = Header.HEIGHT;
    const deviceWidth = isLandscape ? height : width;

    if (!visible) {
      return null;
    }

    return (
      <View style={[{ height: visible ? 'auto' : 0, marginBottom }]}>
        <StatusBar barStyle="light-content" hidden={isLandscape} />
        <View style={styles.headerContainer}>
          {col > 2 && (
            <View
              style={{
                width:
                  centerTitle.length > 0
                    ? deviceWidth * 0.1
                    : deviceWidth * 0.2,
                height: headerHeight,
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
              }}
            >
              {leftIcon ? (
                <Icon
                  onPress={() => navigation.goBack()}
                  style={{
                    fontSize: 36,
                    color: '#ffffff',
                  }}
                  name="chevron-left"
                />
              ) : (
                <Text
                  style={{
                    ...Platform.select({
                      ios: {
                        fontFamily: 'STC',
                        fontWeight: '600',
                      },
                      android: {
                        fontFamily: 'STC-Bold',
                        fontWeight: '400',
                      },
                    }),
                    color: 'white',
                    fontSize: 18,
                    paddingLeft: 10,
                  }}
                >
                  {leftTitle}
                </Text>
              )}
            </View>
          )}
          <View
            style={{
              width:
                centerTitle.length > 0 ? deviceWidth * 0.8 : deviceWidth * 0.7,
              height: headerHeight,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'transparent',
            }}
          >
            <Text
              style={{
                ...Platform.select({
                  ios: {
                    fontFamily: 'STC',
                    fontWeight: '600',
                  },
                  android: {
                    fontFamily: 'STC-Bold',
                    fontWeight: '500',
                  },
                }),
                textAlign: 'center',
                color: 'white',
                fontSize: centerTitle.length > 30 ? 15 : 17,
                paddingLeft: 3,
              }}
            >
              {centerTitle}
            </Text>
          </View>
          <View
            style={{
              width: deviceWidth * 0.1,
              height: headerHeight,
            }}
          >
            {showIconMenu ? (
              <View style={[styles.iconContainer]}>
                <Icon
                  name="more-vertical"
                  onPress={() => {
                    navigation.navigate('Menu');
                  }}
                  style={[styles.icon, styles.iconMenu]}
                />
              </View>
            ) : null}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  statusBar: {
    backgroundColor: '#501f6e',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#501f6e',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.5,
    elevation: 5,
  },
  iconContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    fontSize: 28,
    color: '#fff',
  },
  iconMenu: {
    textAlign: 'center',
    padding: 10,
  },
});

const mapStateToProps = state => ({
  isLandscape: state.user.isLandscape,
});

AppHeader.propTypes = {
  navigation: PropTypes.object,
  isLandscape: PropTypes.bool.isRequired,
  visible: PropTypes.bool,
  leftTitle: PropTypes.string,
  centerTitle: PropTypes.string,
  showIconMenu: PropTypes.bool,
  col: PropTypes.number,
  marginBottom: PropTypes.number,
};

export default connect(
  mapStateToProps,
  {}
)(AppHeader);
