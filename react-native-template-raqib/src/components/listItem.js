import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';
import { View, Text, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { getDimensions } from '../utils/helpers';
import { ItemListTouchable } from '../screens/styled';

const { width } = getDimensions();

export default function ListItem({
  id,
  title,
  subTitle,
  marked = false,
  iconSize = 35,
  showStar = true,
  onIconPress = () => {},
  onPressItem = () => {},
}) {
  return (
    <ItemListTouchable
      width={width * 0.95}
      onPress={() => onPressItem(id)}
      activeColor={marked ? '#CBCACA' : null}
    >
      <View>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.subTitle}>{subTitle}</Text>
      </View>
      {showStar ? (
        <TouchableWithoutFeedback onPress={() => onIconPress(id)}>
          <Icon
            name={marked ? 'ios-star' : 'ios-star-outline'}
            color={marked ? '#447794' : '#fff'}
            size={iconSize}
          />
        </TouchableWithoutFeedback>
      ) : null}
    </ItemListTouchable>
  );
}

const styles = StyleSheet.create({
  listItem: {
    height: 68,
    width: width * 0.95,
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomColor: '#656363',
    borderBottomWidth: 1,
  },
  title: {
    fontSize: 18,
    fontWeight: '400',
  },
  subTitle: {
    fontSize: 14,
    fontWeight: '200',
  },
});

ListItem.propTypes = {
  id: PropTypes.any.isRequired,
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string.isRequired,
  iconSize: PropTypes.number,
  onIconPress: PropTypes.func,
  onPressItem: PropTypes.func,
  marked: PropTypes.bool,
  showStar: PropTypes.bool,
};
