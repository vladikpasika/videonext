import React, { Component } from 'react';
import { StyleSheet, View, Text, FlatList } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Header } from 'react-navigation';

import EventLogItem from './EventLogItem';

const moment = require('moment');

const __ = string => {
  return string;
};

class EventLogsTable extends Component {
  constructor(props) {
    super(props);
  }

  getDateTime = timestamp => {
    return moment(timestamp).format('LLLL');
  };

  filterEventLogs() {
    return Object.keys(this.props.eventsLogs).reduce((filteredLogs, key) => {
      if (this.props.listCameras[this.props.eventsLogs[key].object.objId]) {
        filteredLogs[key] = this.props.eventsLogs[key];
      }
      return filteredLogs;
    }, {});
  }

  render() {
    const {
      deviceWidth,
      deviceHeight,
      listCameras,
      onFlatListEndReached,
      isLandscape,
    } = this.props;
    const width = isLandscape
      ? Math.max(deviceWidth * 0.25, deviceHeight * 0.25)
      : Math.min(deviceWidth * 0.25, deviceHeight * 0.25);
    const headerHeight = Header.HEIGHT;
    const contentHeight = deviceHeight - headerHeight;
    const filteredEventLogs = this.filterEventLogs();
    const eventLogsKeys = Object.keys(filteredEventLogs);
    if (eventLogsKeys.length === 0) {
      return (
        <View style={[styles.noEventContainer, { height: contentHeight }]}>
          <Text style={styles.noData}>{__('No events')}</Text>
        </View>
      );
    }
    return (
      <View>
        <View style={styles.tableHeader}>
          <View style={{ width }}>
            <Text style={styles.hText}>Date</Text>
          </View>
          <View style={{ width: width / 0.9 }}>
            <Text style={styles.hText}>{__('Time')}</Text>
          </View>
          <View style={{ width }}>
            <Text style={styles.hText}>{__('Device')}</Text>
          </View>
          <View style={{ width: width * 0.9 }}>
            <Text style={styles.hText}>{__('Sstm Mssg')}</Text>
          </View>
        </View>
        <View>
          <FlatList
            keyExtractor={key => key}
            data={eventLogsKeys.reverse()}
            onEndReached={onFlatListEndReached}
            onEndReachedThreshold={0.8}
            renderItem={({ item }) => {
              return (
                <EventLogItem
                  keyItem={item}
                  eventsLogs={filteredEventLogs}
                  deviceWidth={deviceWidth}
                  width={width}
                  listCameras={listCameras}
                  logPressHandler={this.props.logPressHandler}
                />
              );
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tableHeader: {
    backgroundColor: '#501f6e',
    flexDirection: 'row',
    borderWidth: 0.7,
    borderColor: '#501f6e',
  },
  hText: {
    color: 'white',
    paddingLeft: 3,
    fontSize: 15,
  },
  logsContainer: {
    flexDirection: 'row',
  },
  col: {
    paddingVertical: 15,
    paddingHorizontal: 5,
    borderRightWidth: 1,
    borderColor: 'black',
  },
  noEventContainer: {
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  noData: {
    color: '#501f6e',
    padding: 5,
    fontSize: 16,
  },
});

EventLogsTable.propTypes = {
  eventsLogs: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  listCameras: PropTypes.object.isRequired,
  logPressHandler: PropTypes.func.isRequired,
  currentCameraTime: PropTypes.object,
  onFlatListEndReached: PropTypes.func,
  isLandscape: PropTypes.bool,
};

EventLogsTable.defaultProps = {
  onFlatListEndReached: () => {},
};

const mapStateToProps = state => ({
  deviceWidth: state.user.deviceWidth,
  deviceHeight: state.user.deviceHeight,
  isLandscape: state.user.isLandscape,
});

export default connect(
  mapStateToProps,
  {}
)(EventLogsTable);
