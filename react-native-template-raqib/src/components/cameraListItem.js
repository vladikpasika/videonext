import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  Image,
  Text,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  View,
} from 'react-native';

export default function CameraListItem({
  id,
  onPressItem,
  image,
  text,
  isLandscape,
  width,
}) {
  const itemWidth = isLandscape ? width * 0.2 - 10 : width * 0.33 - 10;

  return (
    <TouchableOpacity onPress={() => onPressItem(id)}>
      <View style={[styles.listItem, { width: itemWidth }]}>
        {(image && (
          <Fragment>
            <Image
              style={styles.image}
              resizeMode="cover"
              source={{
                uri: image,
              }}
            />
          </Fragment>
        )) || <ActivityIndicator size="large" color="#ffffff" />}
      </View>
      <View style={[styles.textContainer, { width: itemWidth }]}>
        <Text style={styles.text}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  listItem: {
    marginVertical: 5,
    marginHorizontal: 5,
    padding: 5,
    backgroundColor: '#3C3C3C',
  },
  image: {
    height: 100,
  },
  textContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 12,
    color: '#000000',
    backgroundColor: '#ffffff',
    fontFamily: 'STC',
  },
});

CameraListItem.propTypes = {
  id: PropTypes.any.isRequired,
  onPressItem: PropTypes.func.isRequired,
  image: PropTypes.string,
  text: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  isLandscape: PropTypes.bool.isRequired,
};
