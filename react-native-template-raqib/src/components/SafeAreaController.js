import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-navigation';

class SafeAreaController extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const config = {};
    if (this.props.isLandscape) {
      config.vertical = 'always';
    }
    return (
      <SafeAreaView
        style={{ flex: 1, backgroundColor: '#501f6e' }}
        forceInset={config}
      >
        {this.props.children}
      </SafeAreaView>
    );
  }
}
export default connect(
  ({ user: { isLandscape } }) => ({ isLandscape }),
  null
)(SafeAreaController);
