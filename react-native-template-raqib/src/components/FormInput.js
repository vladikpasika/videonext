import React, { Component } from 'react';
import { View, StyleSheet, TextInput, Text, Platform } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather';
import { getDimensions } from '../utils/helpers';

const { width } = getDimensions();

export default class FormInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      borderColor: '#666666',
      borderBottomWidth: 1,
    };
  }

  onFocus = () => {
    this.setState({
      borderColor: '#cc3366',
      borderBottomWidth: 2,
    });
  };

  onBlur = () => {
    this.setState({
      borderColor: '#666666',
      borderBottomWidth: 1,
    });
  };

  render() {
    const { stateName, secureText, value, labelText, updateState } = this.props;
    const { borderColor, borderBottomWidth } = this.state;
    return (
      <View style={styles.inputContainer}>
        <View>
          <Text style={styles.labelText}>{labelText}</Text>
        </View>
        <View>
          <TextInput
            style={[styles.input, { borderColor, borderBottomWidth }]}
            value={value}
            onChangeText={name => updateState(stateName, name)}
            secureTextEntry={secureText}
            autoCapitalize="none"
            underlineColorAndroid="#00000000"
            onFocus={() => this.onFocus()}
            onBlur={() => this.onBlur()}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    flexDirection: 'column',
    paddingHorizontal: 10,
    marginBottom: 10,
    width: width * 0.95,
    backgroundColor: '#fff',
  },
  input: {
    height: 38,
    fontSize: 16,
    paddingLeft: 2,
    width: width * 0.9,
  },
  labelText: {
    color: '#666666',
    fontSize: 17,
    fontWeight: '200',
    ...Platform.select({
      ios: {
        fontFamily: 'STC',
        fontWeight: '500',
      },
      android: {
        fontFamily: 'STC-Bold',
      },
    }),
  },
});

FormInput.propTypes = {
  updateState: PropTypes.func.isRequired,
  labelText: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  secureText: PropTypes.bool.isRequired,
  stateName: PropTypes.string.isRequired,
};
