import React, { PureComponent } from 'react';
import { AppState, DeviceEventEmitter } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getDimensions } from '../../src/utils/helpers';
import {
  changeDeviceOrientation,
  backgroundAuthorization,
} from '../actions/userAction';

const Wrapper = WrappedComponent => {
  class AuthWrapper extends PureComponent {
    constructor(props) {
      super(props);
      this.state = {
        appState: AppState.currentState,
      };
    }

    componentDidMount() {
      AppState.addEventListener('change', this.handleAppStateChange);
      DeviceEventEmitter.addListener(
        'namedOrientationDidChange',
        ({ isLandscape }) => this.detectOrientation(isLandscape)
      );
    }

    componentDidUpdate(prevProps) {
      if (prevProps.loggedIn !== this.props.loggedIn && !this.props.loggedIn) {
        this.props.navigation.navigate('AddSystem');
      }
    }

    componentWillUnmount() {
      DeviceEventEmitter.removeListener('namedOrientationDidChange');
    }

    handleAppStateChange = nextAppState => {
      const { userName, password, serverAddress } = this.props;
      if (
        this.state.appState.match(/inactive|background/) &&
        nextAppState === 'active'
      ) {
        if (userName !== '' && password !== '' && serverAddress !== '') {
          this.backgroundAuth();
        }
      }
      this.setState({ appState: nextAppState });
    };

    backgroundAuth = () => {
      const { serverAddress, userName, password } = this.props;
      const { width, height } = getDimensions();
      this.props.backgroundAuthorization({
        serverAddress,
        userName,
        password,
        width,
        height,
      });
    };

    detectOrientation = isLandscape => {
      const { width, height } = getDimensions();
      this.props.changeDeviceOrientation({
        isLandscape,
        width,
        height,
      });
    };

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  const mapStateToProps = ({
    user: { serverAddress, userName, password, loggedIn },
  }) => ({
    serverAddress,
    userName,
    password,
    loggedIn,
  });

  AuthWrapper.propTypes = {
    serverAddress: PropTypes.string.isRequired,
    userName: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    navigation: PropTypes.object.isRequired,
    changeDeviceOrientation: PropTypes.func.isRequired,
    backgroundAuthorization: PropTypes.func.isRequired,
    loggedIn: PropTypes.bool.isRequired,
  };

  return connect(
    mapStateToProps,
    { changeDeviceOrientation, backgroundAuthorization }
  )(AuthWrapper);
};

export default Wrapper;
