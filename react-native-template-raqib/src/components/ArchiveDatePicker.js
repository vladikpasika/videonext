import React from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity, View } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';

export default function ArchiveDatePicker({
  _handleDatePicked,
  show,
  setCalendar,
}) {
  return (
    <View style={{ flex: 1 }}>
      <DateTimePicker
        isVisible={show}
        onConfirm={_handleDatePicked}
        onCancel={() => setCalendar(false)}
        mode="datetime"
        maximumDate={new Date()}
      />
    </View>
  );
}

ArchiveDatePicker.propTypes = {
  show: PropTypes.bool.isRequired,
  _handleDatePicked: PropTypes.func.isRequired,
  setCalendar: PropTypes.func.isRequired,
};
