import React, { Component } from 'react';
import { View, ActivityIndicator, StyleSheet, Text, Platform } from 'react-native';
import PropTypes from 'prop-types';
import { getDimensions } from '../../utils/helpers';

const { width, height } = getDimensions();
const __ = string => {
  return string;
};

class Spinner extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      show,
      centerMessageBox = true,
      messageText = __('Loading. Please wait...'),
    } = this.props;
    if (show) {
      return (
        <View
          style={[
            styles.container,
            { justifyContent: centerMessageBox ? 'center' : 'flex-end' },
          ]}
        >
          <View
            style={[
              styles.messageContainer,
              { marginBottom: centerMessageBox ? 0 : 75 },
            ]}
          >
            <View style={{ flex: 1 }}>
              <ActivityIndicator size="large" color={'#cc3366'} />
            </View>
            <View style={{ flex: 3 }}>
              <Text style={styles.messageText}>{messageText}</Text>
            </View>
          </View>
        </View>
      );
    }
    return null;
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    height,
    padding: 0,
    margin: 0,
    zIndex: 999,
  },
  messageContainer: {
    flexDirection: 'row',
    backgroundColor: 'white',
    height: 80,
    width: width * 0.9,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.5,
    borderRadius: 3,
  },
  messageText: {
    fontSize: 20,
    color: '#666666',
    fontWeight: '300',
    ...Platform.select({
      ios: {
        fontFamily: 'STC',
        fontWeight: '500',
      },
      android: {
        fontFamily: 'STC-Bold',
        fontWeight: '500',
      },
    }),
  },
});

Spinner.propTypes = {
  show: PropTypes.bool.isRequired,
  centerMessageBox: PropTypes.bool,
  messageText: PropTypes.string,
};

export default Spinner;
