import React from 'react';
import { ImageBackground, StatusBar, StyleSheet } from 'react-native';
import { getDimensions } from '../utils/helpers';
import backgroundImage from '../assets/images/splash.jpg';
import Spinner from './spinner';

const { width, height } = getDimensions();

const Splash = ({ centerMessageBox, messageText }) => (
  <ImageBackground source={backgroundImage} style={styles.container}>
    <StatusBar barStyle="light-content" hidden={true} />
    <Spinner
      show={true}
      centerMessageBox={centerMessageBox}
      messageText={messageText}
    />
  </ImageBackground>
);

const styles = StyleSheet.create({
  container: {
    width,
    height,
  },
});

export default Splash;
