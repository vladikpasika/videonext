import React from 'react';
import { TouchableOpacity, StyleSheet, View, Text } from 'react-native';

const styles = StyleSheet.create({
  logsContainer: {
    flexDirection: 'row',
  },
  col: {
    paddingVertical: 15,
    paddingHorizontal: 5,
    borderRightWidth: 1,
    borderColor: 'black',
  },
});

function EventLogItem({
  keyItem,
  eventsLogs,
  deviceWidth,
  width,
  listCameras,
  logPressHandler,
}) {
  return (
    <View style={{ backgroundColor: eventsLogs[keyItem].bgColor }}>
      <TouchableOpacity
        id={keyItem}
        style={[
          styles.logsContainer, //fix color issue
          {
            width: deviceWidth,
          },
        ]}
        onPress={() => logPressHandler(keyItem)}
      >
        <View style={[styles.col, { width }]}>
          <Text>{eventsLogs[keyItem].when.date}</Text>
        </View>
        <View style={[styles.col, { width: width / 0.9 }]}>
          <Text>{eventsLogs[keyItem].when.time}</Text>
        </View>
        <View style={[styles.col, { width }]}>
          <Text>{listCameras[eventsLogs[keyItem].object.objId].name}</Text>
        </View>
        <View style={[styles.col, { width: width * 0.9 }]}>
          <Text>{eventsLogs[keyItem].message}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

export default EventLogItem;
