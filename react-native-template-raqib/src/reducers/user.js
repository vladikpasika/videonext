import { Dimensions } from 'react-native';
import { types } from '../actions/userAction';

const { width, height } = Dimensions.get('window');
console.log(width, height);
export const initialState = {
  error: null,
  loading: false,
  loggedIn: false,
  encryptionKey: '',
  serverName: '',
  serverAddress: '',
  userName: '',
  password: '',
  userData: '',
  mainSystem: null,
  systems: {},
  isLandscape: false,
  deviceWidth: width,
  deviceHeight: height,
};

export default function user(state = initialState, action = {}) {
  switch (action.type) {
    case types.LOGIN_INFO.REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case types.LOGIN_INFO.SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        encryptionKey: action.info.data.loginInfo.encryptionKey,
      };
    case types.BACKGROUND_AUTHORIZATION:
      return {
        ...state,
        loading: true,
        error: null,
        deviceWidth: action.width,
        deviceHeight: action.height,
      };
    case types.LOGIN_INFO.FAILURE:
      return {
        ...state,
        loading: false,
        error: action.message,
      };
    case types.LOGIN.REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case types.LOGIN.FAILURE:
      return {
        ...state,
        loading: false,
        error: action.message,
      };
    case types.CHANGE_MAIN_SYSTEM:
      return {
        ...state,
        mainSystem: action.payload,
      };
    case types.CHANGE_DEVICE_ORIENTATION:
      return {
        ...state,
        isLandscape: action.isLandscape,
        deviceWidth: action.width,
        deviceHeight: action.height,
      };
    case types.LOGIN.SUCCESS: {
      const {
        sysName,
        sysAddress,
        userName,
        password,
        data,
        systemInfo,
      } = action;
      const address = sysAddress.replace(/(^\w+:|^)\/\//, '');
      return {
        ...state,
        loggedIn: true,
        loading: false,
        error: null,
        serverName: sysName,
        serverAddress: sysAddress,
        mainSystem: address,
        userName: userName,
        password: password,
        userData: data,
        systems: {
          ...state.systems,
          [address]: systemInfo,
        },
      };
    }
    case types.LOGOUT.SUCCESS:
      return {
        ...state,
        loggedIn: false,
        loading: false,
        encryptionKey: '',
        userName: '',
        password: '',
        userData: '',
        error: null,
      };
    case 'persist/PURGE':
      return initialState;
    default:
      return state;
  }
}
