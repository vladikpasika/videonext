import { combineReducers } from 'redux';
import user from './user';
import cameras from './cameras';

export default combineReducers({
  user,
  cameras,
});
