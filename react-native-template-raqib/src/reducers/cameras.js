import { types } from '../actions/cameras';
import { types as userTypes } from '../actions/userAction';

const initialState = {
  playerUri: '',
  currentSet: '',
  currentCamera: null,
  currentCameraTime: {},
  list: {},
  sets: {},
  listAttr: {},
  eventLogs: {},
  loading: false,
  mediaUrlLoading: false,
  ptz: {},
};

export default function cameras(state = initialState, action = {}) {
  switch (action.type) {
    case types.GET_CURRENT_CAMERA_SET.SUCCESS:
      return {
        ...state,
        list: { ...action.payload.list },
        currentSet: action.payload.currentSet,
      };
    case types.SET_CURRENT_CAMERA:
      return {
        ...state,
        currentCamera: action.payload,
        currentCameraTime: {},
      };
    case types.SET_CURRENT_SET:
      return {
        ...state,
        currentSet: action.payload,
      };
    case types.SET_CURRENT_CAMERA_TIME:
      return {
        ...state,
        currentCameraTime: action.payload,
      };
    case types.GET_EVENT_LOG.REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.GET_EVENT_LOG.SUCCESS: {
      const { eLogs, camerasList } = action.payload;
      let eventLogs;
      if (action.meta === 'clearLogs' || action.meta === 'loadLogs') {
        eventLogs = { ...eLogs };
      } else {
        eventLogs = { ...state.eventLogs, ...eLogs };
      }
      return {
        ...state,
        eventLogs,
        list: camerasList,
        loading: false,
      };
    }
    case types.GET_EVENT_LOG.FAILURE:
      return {
        ...state,
        loading: false,
      };
    case types.GET_EVENT_LOG.CLEAR:
      return {
        ...state,
        eventLogs: {},
      };
    case types.GET_CAMERA_EVENT_LOG.REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.GET_CAMERA_EVENT_LOG.SUCCESS: {
      let eventLogs;
      if (action.meta === 'clearLogs' || action.meta === 'loadLogs') {
        eventLogs = { ...action.payload };
      } else {
        eventLogs = { ...state.eventLogs, ...action.payload };
      }
      return {
        ...state,
        eventLogs,
        loading: false,
      };
    }
    case types.GET_CAMERA_EVENT_LOG.FAILURE:
      return {
        ...state,
      };
    case types.GET_CAMERA_ATTRIBUTES.REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.GET_CAMERA_ATTRIBUTES.SUCCESS:
      return {
        ...state,
        listAttr: action.payload,
        loading: false,
      };
    case types.GET_CAMERA_ATTRIBUTES.FAILURE:
      return {
        ...state,
        loading: false,
      };
    case userTypes.LOGIN_INFO.SUCCESS:
      return {
        ...state,
      };
    case userTypes.CHANGE_MAIN_SYSTEM:
      return {
        ...state,
        sets: {},
        list: {},
        listAttr: {},
        eventLogs: {},
        playerUri: '',
        currentSet: '',
        currentCamera: null,
        currentCameraTime: {},
      };
    case types.GET_SETS.REQUEST:
      return {
        ...state,
        loader: true,
      };
    case types.GET_SETS.FAILURE:
      return {
        ...state,
        loader: false,
      };
    case types.GET_SETS.SUCCESS:
      return {
        ...state,
        sets: action.payload,
        loader: false,
      };
    case types.GET_MEDIA_URL.REQUEST:
      return {
        ...state,
        loading: true,
        mediaUrlLoading: true,
      };
    case types.GET_MEDIA_URL.SUCCESS:
      return {
        ...state,
        playerUri: action.payload,
        loading: false,
        mediaUrlLoading: false,
      };
    case types.GET_MEDIA_URL.FAILURE:
      return {
        ...state,
        loading: false,
      };
    case types.PTZ_EVENT.SAVE_REQUEST_ID:
      return {
        ...state,
        ptz: {
          lastRequestId: action.requestId,
        },
      };
    case types.RESET_CAMERAS:
      return initialState;
    case 'persist/PURGE':
      return initialState;
    default:
      return state;
  }
}
