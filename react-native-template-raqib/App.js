import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import configureStore from './src/store/configure';
import { SwitchNavigator } from './src/navigations/AppNavigator';
import Splash from './src/components/Splash';
import SafeAreaController from './src/components/SafeAreaController.js';
import CONFIG from 'react-native-config';

console.disableYellowBox = true;
const { store, persistor } = configureStore();
const __ = string => {
  return string;
};

class VideoNextApp extends React.Component {
  constructor() {
    super();
    this.state = {
      reHydrated: false,
    };
  }

  componentDidMount() {
    persistStore(store, {}, () => {
      this.setState({ reHydrated: true });
    });
  }

  render() {
    const { reHydrated } = this.state;
    if (!reHydrated) {
      return (
        <Splash
          centerMessageBox={false}
          messageText={__(
            `Welcome to ${CONFIG.SERVER_NAME} \nLoading. Please, wait...`
          )}
        />
      );
    }
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <SafeAreaController>
            <SwitchNavigator />
          </SafeAreaController>
        </PersistGate>
      </Provider>
    );
  }
}

AppRegistry.registerComponent('Raqib', () => VideoNextApp);

export default VideoNextApp;
export const createdPersistor = persistor;
