import { AppRegistry } from 'react-native';
import App from './App';
/* eslint-disable */
// To see all the requests in the chrome Dev tools in the network tab.
XMLHttpRequest =
    __DEV__ && GLOBAL.originalXMLHttpRequest
        ? GLOBAL.originalXMLHttpRequest
        : GLOBAL.XMLHttpRequest;

AppRegistry.registerComponent('app', () => App);
