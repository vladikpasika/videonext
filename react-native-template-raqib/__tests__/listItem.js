import React from 'react';
import path from 'path';
import { shallow } from 'enzyme';
import fakeProps from 'react-fake-props';
import ListItem from '../src/components/listItem';

describe('Test <ListItem />', () => {
  it('To match snapshot', () => {
    const componentPath = path.join(__dirname, '../src/components/listItem.js');
    const props = fakeProps(componentPath);
    const wrapper = shallow(<ListItem {...props} />);
    expect(wrapper).toMatchSnapshot();
    wrapper.setProps({ id: 2 });
    expect(wrapper).toMatchSnapshot();
  });
});
