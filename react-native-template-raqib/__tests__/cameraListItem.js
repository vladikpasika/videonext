import React from 'react';
import { shallow } from 'enzyme';
import CameraListItem from '../src/components/cameraListItem';

describe('Test <CameraListItem />', () => {
  it('Render <ActivityIndicator /> if there is not image property', () => {
    const wrapper = shallow(
      <CameraListItem id={1} onPressItem={() => {}} text="test" />
    );
    expect(wrapper.find('ActivityIndicator')).toBeDefined();
  });

  it('Render <Image /> if there is image property', () => {
    const wrapper = shallow(
      <CameraListItem
        id={1}
        onPressItem={() => {}}
        text="test"
        image="someurl"
      />
    );
    expect(wrapper.find('Image')).toBeDefined();
  });

  it('Call item, with receiving id', () => {
    const id = Math.random();
    const onPressItem = jest.fn(value => value);
    const wrapper = shallow(
      <CameraListItem
        id={id}
        onPressItem={onPressItem}
        text="test"
        image="someurl"
      />
    );
    wrapper.find('TouchableOpacity').simulate('press');
    expect(onPressItem).toBeCalledWith(id);
  });

  it('Text property is displayed', () => {
    const text = 'text';
    const wrapper = shallow(
      <CameraListItem
        id={1}
        onPressItem={() => {}}
        text={text}
        image="someurl"
      />
    );
    expect(
      wrapper
        .find('Text')
        .dive()
        .text()
    ).toEqual(text);
  });
});
