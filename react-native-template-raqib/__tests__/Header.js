import 'jsdom-global/register';
import React from 'react';
import path from 'path';
import { shallow } from 'enzyme';
import fakeProps from 'react-fake-props';
import configureStore from 'redux-mock-store';
import Header from '../src/components/Header';

const middlewares = []; // you can mock any middlewares here if necessary
const mockStore = configureStore(middlewares);

const initialState = {
  user: { isLandscape: true },
};

describe('Test <Header />', () => {
  it('Display leftIcon if showLeftIcon property is true', () => {
    const componentPath = path.join(__dirname, '../src/components/Header.js');
    const props = fakeProps(componentPath);
    expect(
      shallow(<Header {...props} showLeftIcon />, {
        context: { store: mockStore(initialState) },
      }).find('TouchableWithoutFeedback')
    ).toBeDefined();
  });
  it("Display text of property (title) when it's mentioned", () => {
    const componentPath = path.join(__dirname, '../src/components/Header.js');
    const props = fakeProps(componentPath);
    expect(
      shallow(<Header {...props} />, {
        context: { store: mockStore(initialState) },
      })
        .dive()
        .find('Text')
    ).toHaveLength(1);
  });
  it('Show icon when showIcons property is true', () => {
    const componentPath = path.join(__dirname, '../src/components/Header.js');
    const props = fakeProps(componentPath);
    expect(
      shallow(<Header {...props} showLeftIcon />, {
        context: { store: mockStore(initialState) },
      })
        .dive()
        .find('Icon')
        .findWhere(n => n.prop('name') === 'menu')
        .exists()
    ).toBeTruthy();
  });
  it('Status bar hides when isLandscape property is true', () => {
    const componentPath = path.join(__dirname, '../src/components/Header.js');
    const isLandscape = true;
    const props = fakeProps(componentPath);
    expect(
      shallow(<Header {...props} isLandscape={isLandscape} />, {
        context: { store: mockStore(initialState) },
      })
        .dive()
        .find('StatusBar')
        .prop('hidden')
    ).toEqual(isLandscape);
  });
  it('Navigation go back when press on <TouchableWithoutFeedback>', () => {
    const componentPath = path.join(__dirname, '../src/components/Header.js');
    const navigation = {
      goBack: jest.fn(),
    };
    const props = fakeProps(componentPath);
    shallow(<Header {...props} showLeftIcon navigation={navigation} />, {
      context: { store: mockStore(initialState) },
    })
      .dive()
      .find('TouchableWithoutFeedback')
      .simulate('press');
    expect(navigation.goBack).toBeCalled();
  });
  it('Navigate to Menu when there is property showIcons and icon is pressed', () => {
    const componentPath = path.join(__dirname, '../src/components/Header.js');
    const navigation = {
      navigate: jest.fn(path => path),
    };
    const props = fakeProps(componentPath);
    shallow(<Header {...props} showIcons navigation={navigation} />, {
      context: { store: mockStore(initialState) },
    })
      .dive()
      .find('Icon')
      .simulate('press');
    expect(navigation.navigate).toHaveBeenCalledWith('Menu');
  });
});
