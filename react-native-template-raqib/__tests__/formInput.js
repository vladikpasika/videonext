import React from 'react';
import path from 'path';
import { shallow } from 'enzyme';
import fakeProps from 'react-fake-props';
import FormInput from '../src/components/FormInput';

describe('Test <FormInput />', () => {
  it('(placeholder, value, secureTextEntry) exists as props if they are filled in component', () => {
    const text = 'text';
    const componentPath = path.join(
      __dirname,
      '../src/components/FormInput.js'
    );
    const props = fakeProps(componentPath);
    const wrapper = shallow(<FormInput {...props} iconName="activity" />);
    const componentProps = wrapper.find('TextInput').props();
    expect(componentProps.placeholder).toEqual(props.placeHolderText);
    expect(componentProps.value).toEqual(props.value);
    expect(componentProps.secureTextEntry).toEqual(props.secureText);
  });
  it('updateState prop is called with proper value, while changing input value', () => {
    const insertValue = 'insertValue';
    const func = jest.fn();
    const componentPath = path.join(
      __dirname,
      '../src/components/FormInput.js'
    );
    const props = fakeProps(componentPath);
    shallow(<FormInput {...props} iconName="activity" updateState={func} />)
      .find('TextInput')
      .dive()
      .simulate('changeText', insertValue);
    expect(func).toHaveBeenCalledWith(props.stateName, insertValue);
  });
  it('Show required symbol, when component has property "required"', () => {
    const componentPath = path.join(
      __dirname,
      '../src/components/FormInput.js'
    );
    const props = fakeProps(componentPath);
    expect(
      shallow(<FormInput {...props} iconName="activity" required />)
        .find('Text')
        .dive()
        .text()
    ).toEqual('*');
  });
  it('Hide required symbol, when component has\'t property "required"', () => {
    const componentPath = path.join(
      __dirname,
      '../src/components/FormInput.js'
    );
    const props = fakeProps(componentPath);
    expect(
      shallow(<FormInput {...props} iconName="activity" required={false} />)
        .dive()
        .find('Text')
    ).not.toBe('Text');
  });
});
