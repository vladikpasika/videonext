module.exports = {
  // Placeholder used to rename and replace in files
  // package.json, index.json, android/, ios/
  placeholderName: "Raqib",

  // Directory with template
  templateDir: "./react-native-template-raqib",

  // Path to script, which will be executed after init
  // postInitScript: "./script.js"
};
